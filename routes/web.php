<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\CommonController;

Route::get('/', [CommonController::class, 'homepage'])->name('homepage');

Route::get('/reference', function() {
    return view('reference');
});

Route::prefix('docs')->group(function () {
    Route::prefix('getting-started')->group(function () {
        Route::get('/', function () {
            return view('docs.getting-started');
        })->name('docs.getting-started');

        Route::get('/contribute', function () {
            return view('docs.contribute');
        })->name('docs.contribute');

        Route::get('/upgrade', function () {
            return view('docs.upgrade');
        })->name('docs.upgrade');
    });

    Route::prefix('components')->group(function () {
        Route::get('/element', function () {
            return view('docs.element');
        })->name('docs.element');

        Route::get('/text', function () {
            return view('docs.text');
        })->name('docs.text');

        Route::get('/navbar', function () {
            return view('docs.navbar');
        })->name('docs.navbar');

        Route::get('/button', function () {
            return view('docs.button');
        })->name('docs.button');

        Route::get('/link', function () {
            return view('docs.link');
        })->name('docs.link');
    });

    Route::prefix('forms')->group(function () {
        Route::get('/overview', function () {
            return view('docs.forms');
        })->name('docs.forms');

        Route::get('/field', function () {
            return view('docs.field');
        })->name('docs.field');

        Route::get('/input', function () {
            return view('docs.input');
        })->name('docs.input');

        Route::get('/file', function () {
            return view('docs.file');
        })->name('docs.file');

        Route::get('/select', function () {
            return view('docs.select');
        })->name('docs.select');

        Route::get('/check-radio', function () {
            return view('docs.check-radio');
        })->name('docs.check-radio');

        Route::get('/autoread', function () {
            return view('docs.autoread');
        })->name('docs.autoread');
    });

    Route::prefix('containers')->group(function () {
        Route::get('/overview', function () {
            return view('docs.containers');
        })->name('docs.containers');

        Route::get('/button-group', function () {
            return view('docs.button-group');
        })->name('docs.button-group');

        Route::get('/modal', function () {
            return view('docs.modal');
        })->name('docs.modal');

        Route::get('/accordion', function () {
            return view('docs.accordion');
        })->name('docs.accordion');

        Route::get('/collapse', function () {
            return view('docs.collapse');
        })->name('docs.collapse');

        Route::get('/tabs', function () {
            return view('docs.tabs');
        })->name('docs.tabs');

        Route::get('/enclose', function () {
            return view('docs.enclose');
        })->name('docs.enclose');
    });

    Route::prefix('custom-elements')->group(function () {
        Route::get('/overview', [CommonController::class, 'customIndex'])->name('docs.custom-elements');
        Route::get('/examples', [CommonController::class, 'customExamples'])->name('docs.custom-examples');
    });

    Route::prefix('more')->group(function () {
        Route::get('/translations', function () {
            return view('docs.translations');
        })->name('docs.translations');

        Route::get('/accessibility', function () {
            return view('docs.accessibility');
        })->name('docs.accessibility');
    });

    Route::prefix('about')->group(function () {
        Route::get('/overview', function () {
            return view('docs.about');
        })->name('docs.about');

        Route::get('/changelog', function () {
            return view('docs.changelog');
        })->name('docs.changelog');

        Route::get('/alternatives', function () {
            return view('docs.alternatives');
        })->name('docs.alternatives');

        /*
            Old route
        */
        Route::get('/donate', function () {
            return redirect()->route('docs.contribute');
        })->name('docs.donate');
    });
});

Route::get('/recipes', function() {
    return redirect()->route('docs.custom-examples');
});

Route::get('/temp_test', function () {
    return view('temp.test');
})->name('temp.test');

Route::post('/temp_test', [CommonController::class, 'tempTest'])->name('temp.test.post');

/*
    Those are used in examples
*/
Route::post('/user', [CommonController::class, 'fakePost'])->name('user.store');
Route::put('/user/{id}', [CommonController::class, 'fakePost'])->name('user.update');
Route::post('/document/save', [CommonController::class, 'fakePost'])->name('document.store');
Route::post('/do/nothing', [CommonController::class, 'fakePost'])->name('nothing');
Route::post('/validated', [CommonController::class, 'validatedRoute'])->name('validated');
Route::post('/validated_dot', [CommonController::class, 'validatedDotRoute'])->name('validated_dot');
Route::post('/autoread_validate', [CommonController::class, 'autoreadValidation'])->name('autoread_validate');
Route::post('/autoread_save', [CommonController::class, 'autoreadSaving'])->name('autoread_save');
