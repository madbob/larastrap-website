<?php

return [
    'prefix' => 'ls',

    'commons' => [
        'label_width' => '4',
        'input_width' => '8',
    ],

    'elements' => [
        'navbar' => [
            'color' => 'light',
        ],

        'radios' => [
            'color' => 'outline-primary',
        ],

        'checks' => [
            'color' => 'outline-primary',
        ],

        'tabs' => [
            'tabview' => 'tabs',
        ],

        'tabpane' => [
            'classes' => ['p-3']
        ]
    ],

    'customs' => [
        'command' => [
            'extends' => 'text',
            'params' => [
                'classes' => ['command'],
                'attributes' => ['readonly' => 'readonly'],
                'textappend' => '<i class="bi bi-clipboard"></i>',
                'squeeze' => true,
            ],
        ],

        'title' => [
            'extends' => 'link',
            'params' => [
                'override_classes' => ['title-link'],
                'reviewCallback' => function($node, $params) {
                    $label = $params['label'];
                    $identifier = Str::slug($params['label']);
                    $params['id'] = $identifier;
                    $params['label'] = sprintf('<h2>%s</h2>', $params['label']);
                    $params['href'] = sprintf('#%s', $identifier);
                    app()->make('submenu-builder')->push($label, $params['href']);
                    return $params;
                }
            ],
        ],

        'mybutton' => [
            'extends' => 'button',
            'params' => [
                'color' => 'warning',
            ]
        ],

        'element' => [
            'extends' => 't',
            'params' => [
                'classes' => ['text-danger'],
                'node' => 'strong',
            ]
        ],

        'value' => [
            'extends' => 't',
            'params' => [
                'node' => 'code',
            ]
        ],

        'code' => [
            'extends' => 't',
            'params' => [
                'node' => 'code',
            ]
        ],

        'parameter' => [
            'extends' => 't',
            'params' => [
                'node' => 'strong',
            ]
        ]
    ],
];
