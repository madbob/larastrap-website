import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import path from 'path';
import purge from '@erbelion/vite-plugin-laravel-purgecss';

export default defineConfig({
    build: {
        assetsInlineLimit: 0,
        sourcemap: true,
    },
    plugins: [
        laravel({
            input: [
                'resources/sass/app.scss',
                'resources/js/app.js',
            ],
            refresh: true,
        }),

        /*
        purge({
            templates: ['blade'],
        }),
        */
    ],
    resolve: {
        alias: {
            '~bootstrap': path.resolve(__dirname, 'node_modules/bootstrap'),
            '~bootstrap-icons': path.resolve(__dirname, 'node_modules/bootstrap-icons'),
        }
    },
});
