[
    'form' => [
        'formview' => 'vertical',
        'buttons_align' => 'start',
        'buttons' => [
            [
                'color' => 'danger',
                'label' => 'Save',
                'attributes' => ['type' => 'submit']
            ]
        ]
    ],

    'field' => [
        'label_class' => 'text-danger',
    ]
]
