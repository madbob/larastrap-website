[
    'form' => [
        'formview' => 'vertical',
        'buttons_align' => 'start',
        'buttons' => [
            [
                'color' => 'danger',
                'label' => 'Submit',
                'classes' => ['mt-5', 'btn-lg'],
                'attributes' => ['type' => 'submit']
            ]
        ]
    ],

    'field' => [
        'label_class' => 'text-danger display-5',
    ],

    'text' => [
        'classes' => ['form-control-lg'],
    ],

    'email' => [
        'classes' => ['form-control-lg'],
    ]
]
