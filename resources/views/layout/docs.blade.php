@extends('layout.app', [
    'title' => $title,
    'claim' => $claim,
])

@section('contents')

@php

$menu = [
    '<i class="bi bi-book-half" style="color: var(--bs-indigo);"></i> Intro' => [
        'Getting Started' => route('docs.getting-started'),
        'Contribute' => route('docs.contribute'),
        'Upgrade' => route('docs.upgrade'),
    ],
    '<i class="bi bi-menu-button-wide-fill" style="color: var(--bs-cyan);"></i> Components' => [
        'Base Element' => route('docs.element'),
        'Typography' => route('docs.text'),
        'Navbar' => route('docs.navbar'),
        'Buttons' => route('docs.button'),
        'Links' => route('docs.link'),
    ],
    '<i class="bi bi-ui-radios" style="color: var(--bs-blue);"></i> Forms' => [
        'Overview' => route('docs.forms'),
        'Fields' => route('docs.field'),
        'Inputs' => route('docs.input'),
        'File' => route('docs.file'),
        'Selects' => route('docs.select'),
        'Checks and Radios' => route('docs.check-radio'),
        'Auto Read' => route('docs.autoread'),
    ],
    '<i class="bi bi-grid-fill" style="color: var(--bs-teal);"></i> Containers' => [
        'Overview' => route('docs.containers'),
        'Button Groups' => route('docs.button-group'),
        'Modal' => route('docs.modal'),
        'Accordion' => route('docs.accordion'),
        'Collapse' => route('docs.collapse'),
        'Tabs' => route('docs.tabs'),
        'Enclose' => route('docs.enclose'),
    ],
    '<i class="bi bi-tools" style="color: var(--bs-blue);"></i> Custom Elements' => [
        'Overview' => route('docs.custom-elements'),
        'Examples' => route('docs.custom-examples'),
    ],
    '<i class="bi bi-braces-asterisk" style="color: var(--bs-red);"></i> More' => [
        'Translations' => route('docs.translations'),
        'Accessibility' => route('docs.accessibility'),
    ],
    '<i class="bi bi-globe2" style="color: var(--bs-indigo);"></i> About' => [
        'Overview' => route('docs.about'),
        'Changelog' => route('docs.changelog'),
        'Alternatives' => route('docs.alternatives'),
    ],
];

$internal_menu = app()->make('submenu-builder')->links;
$currenturl = url()->current();

@endphp

<div class="container mt-3">
    <div class="row mb-5">
        <div class="d-none d-lg-block col-lg-2 d-lg-block ps-0">
            <ul id="docs-menu" class="list-group sticky-top">
                @foreach($menu as $header => $children)
                    <li class="py-2">
                        <strong>{!! $header !!}</strong>
                        <ul class="list-unstyled">
                            @foreach($children as $label => $route)
                                <li>
                                    <a href="{{ $route }}" class="d-inline-block {{ $currenturl == $route ? 'active' : '' }}">{{ $label }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endforeach
            </ul>
        </div>

        <div class="col-12 col-lg-8" data-bs-spy="scroll" data-bs-target="#local-menu" data-bs-smooth-scroll="true">
            <div class="dropdown-center mb-2 d-block d-lg-none">
                <button class="btn btn-primary dropdown-toggle w-100" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Explore the Docs
                </button>
                <ul class="dropdown-menu vw-100 bg-dark">
                    @foreach($menu as $header => $children)
                        <li><h6 class="dropdown-header">{!! $header !!}</h6></li>
                        @foreach($children as $label => $route)
                            <li><a href="{{ $route }}" class="dropdown-item text-white">{{ $label }}</a></li>
                        @endforeach
                    @endforeach
                </ul>
            </div>

            @if(empty($internal_menu) == false)
                <div class="dropdown-center mb-5 d-block d-lg-none">
                    <button class="btn btn-light dropdown-toggle w-100" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                        On this Page
                    </button>
                    <ul class="dropdown-menu vw-100">
                        @foreach(app()->make('submenu-builder')->links as $label => $anchor)
                            <li><a href="{{ $anchor }}" class="dropdown-item">{{ $label }}</a></li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if(isset($repo_filepath))
                <a target="_blank" href="{{ $repo_filepath }}" class="btn btn-sm rounded-2 float-lg-end mb-3 btn-outline-secondary">View on GitLab</a>
            @endif

            @yield('docs')
        </div>

        <div class="d-none d-lg-block col-lg-2 d-lg-block ps-4 pe-0">
            @if(empty($internal_menu) == false)
                <ul id="local-menu" class="list-unstyled sticky-top">
                    <li>
                        <strong class="h6">On this page</strong>
                        <hr>
                    </li>

                    @foreach($internal_menu as $label => $anchor)
                        <li>
                            <a href="{{ $anchor }}" class="list-group-item list-group-item-action">{{ $label }}</a>
                        </li>
                    @endforeach
                </ul>
            @endif
        </div>
    </div>
</div>

@endsection
