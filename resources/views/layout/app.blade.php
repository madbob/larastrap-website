<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>{{ $title }}</title>
        <meta name="description" content="{{ $claim }}">

        @vite(['resources/sass/app.scss', 'resources/js/app.js'])

        <link rel="icon" href="{{ Vite::asset('resources/images/icons/favicon.ico') }}" sizes="any">
        <link rel="apple-touch-icon" href="{{ Vite::asset('resources/images/icons/apple-touch-icon.png') }}">

        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:title" content="{{ $title }}">
        <meta name="twitter:description" content="{{ $claim }}">
        <meta name="twitter:creator" content="@madbob">
        <meta name="twitter:image" content="https://larastrap.madbob.org/images/fb.png">

        <meta property="og:site_name" content="Larastrap" />
        <meta property="og:title" content="Larastrap" />
        <meta property="og:url" content="https://larastrap.madbob.org/" />
        <meta property="og:image" content="https://larastrap.madbob.org/images/fb.png" />
        <meta property="og:type" content="website" />
        <meta property="og:locale" content="en" />

        <script type="application/ld+json">
        {
            "@context" : "http://schema.org",
            "@type" : "WebApplication",
            "name" : "Larastrap",
            "abstract": "Opinionated Bootstrap 5 components for Laravel",
            "author" : {
                "@type" : "Person",
                "name" : "Roberto Guido",
                "sameAs": "https://www.madbob.org/"
            },
            "offers": {
                "@type" : "Offer",
                "price": 0,
                "priceCurrency": "EUR",
                "name": "Free and Open Source"
            },
            "applicationCategory" : ["DeveloperApplication", "https://www.wikidata.org/wiki/Q1330336"],
            "operatingSystem" : "web",
            "url" : "https://larastrap.madbob.org/",
            "keywords": "laravel, bootstrap, components",
            "license": "https://spdx.org/licenses/MIT.html",
            "about": [
                {
                    "@type" : "Thing",
                    "name": "Laravel",
                    "sameAs": "https://www.wikidata.org/wiki/Q13634357"
                },
                {
                    "@type" : "Thing",
                    "name": "Bootstrap",
                    "sameAs": "https://www.wikidata.org/wiki/Q893195"
                }
            ],
            "review": {
                "@type" : "Review",
                "reviewRating": {
                    "@type": "Rating",
                    "ratingValue": "5"
                },
                "author": {
                    "@type" : "Person",
                    "name" : "Roberto Guido"
                }
            }
        }
        </script>
    </head>
    <body>
        @php

        $endlogos = [
            '<i class="bi bi-gitlab"></i>' => ['url' => 'https://gitlab.com/madbob/larastrap']
        ];

        @endphp

        <x-larastrap::navbar title="Larastrap" color="dark" classes="main-navbar" :title_link="route('homepage')" container="normal" :options="[
            'Home' => ['route' => 'homepage'],
            'Docs' => ['route' => 'docs.getting-started'],
        ]" :end_options="$endlogos" />

        @yield('contents')

        <footer class="bg-light py-5 text-body-secondary">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-8">
                        <p class="h2 mb-4 text-black">Larastrap</p>

                        <p>
                            Built with love and hammer by <a href="http://madbob.org/"><img src="{{ url('/images/mad.png') }}" alt="MAD"></a> and <a href="https://gitlab.com/madbob/larastrap/-/graphs/master?ref_type=heads">many contributors</a>.
                        </p>
                        <p>
                            Code licensed <a href="https://gitlab.com/madbob/larastrap/-/blob/master/LICENSE.txt?ref_type=heads">MIT</a>.
                        </p>
                    </div>
                    <div class="col-12 col-lg-2">
                        <p class="h5 mb-4">Links</p>

                        <ul class="list-unstyled">
                            <li><a href="{{ route('homepage') }}">Home</a></li>
                            <li><a href="{{ route('docs.getting-started') }}">Getting Started</a></li>
                        </ul>
                    </div>
                    <div class="col-12 col-lg-2">
                        <p class="h5 mb-4">Community</p>

                        <ul class="list-unstyled">
                            <li><a href="https://gitlab.com/madbob/larastrap">Code Repository</a></li>
                            <li><a href="https://gitlab.com/madbob/larastrap/-/issues">Issues</a></li>
                            <li><a href="{{ route('docs.contribute', ['#donations']) }}">Donate</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>

        @if(config('app.debug') == false)
            <!-- Matomo -->
            <script type="text/javascript">
                var _paq = window._paq || [];
                /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
                _paq.push(["setDoNotTrack", true]);
                _paq.push(["disableCookies"]);
                _paq.push(['trackPageView']);
                _paq.push(['enableLinkTracking']);
                (function() {
                var u="//stats.madbob.org/";
                _paq.push(['setTrackerUrl', u+'matomo.php']);
                _paq.push(['setSiteId', '19']);
                var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
                g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
                })();
            </script>
            <noscript><p><img src="//stats.madbob.org/matomo.php?idsite=19&amp;rec=1" style="border:0;" alt="" /></p></noscript>
            <!-- End Matomo Code -->
        @endif
    </body>
</html>
