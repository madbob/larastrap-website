@extends('layout.docs', [
    'title' => 'Accessibility | Larastrap',
    'claim' => 'Accessible Laravel applications with Bootstrap',
])

@section('docs')

<h1>Accessibility</h1>

<p class="mt-4">
    Larastrap provides to automatically generate many <a href="https://www.w3.org/WAI/standards-guidelines/aria/">WAI-ARIA</a> attributes, in particular those suggested by the Bootstrap's documentation for each component.
</p>

<p>
    It is possible to overwrite all of them by just assigning an explicit value into the <x-larastrap::parameter>attributes</x-larastrap::parameter> array passed to each component.
</p>

@include('partials.example', ['snippet' => 'accessibility.base'])

<x-larastrap::title label="Inputs" />

<p>
    In most situations, when <a href="{{ route('docs.input') }}">Input elements</a> have not a visible label (e.g. when <x-larastrap::parameter>squeeze</x-larastrap::parameter> is applied), an <x-larastrap::element>aria-label</x-larastrap::element> attribute is added with the content of the <x-larastrap::parameter>label</x-larastrap::parameter> or <x-larastrap::parameter>placeholder</x-larastrap::parameter> parameters.
</p>
<p>
    When using the <x-larastrap::parameter>help</x-larastrap::parameter> parameter, the input box is labelled with <x-larastrap::element>aria-describedby</x-larastrap::element>.
</p>

<x-larastrap::title label="Buttons and Links" />

<p>
    When using the <x-larastrap::parameter>disabled</x-larastrap::parameter> parameter, both buttons and links are labelled with <x-larastrap::element>aria-disabled="true"</x-larastrap::element>.
</p>

<x-larastrap::title label="Tabs" />

<p>
    Both <x-larastrap::element>aria-labelledby</x-larastrap::element> and <x-larastrap::element>aria-current</x-larastrap::element> attributes are automatically added to each tab.
</p>

<x-larastrap::title label="Accordions" />

<p>
    Both <x-larastrap::element>aria-labelledby</x-larastrap::element> and <x-larastrap::element>aria-expanded</x-larastrap::element> attributes are automatically added to each panel.
</p>

<p>
    Buttons have a proper <x-larastrap::element>aria-controls</x-larastrap::element> attribute, referring the related panel.
</p>

@endsection
