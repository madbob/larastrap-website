@extends('layout.docs', [
    'title' => 'Inputs | Larastrap',
    'claim' => 'How to use the Boostrap5 Input components in Laravel',
])

@section('docs')

<h1>Inputs</h1>

<p>
    Larastrap provides many components implementing the many HTML input types.
</p>

@include('partials.example', ['snippet' => 'inputs.base'])

<p class="mt-4">
    All parameter assignable to the <a href="{{ route('docs.field') }}">Field element</a> are also directly appliable to Input elements: those will infer with the nodes wrapping the actual input component.
</p>

@include('partials.example', ['snippet' => 'inputs.field'])

<p class="mt-4">
    Many of the Input components in Larastrap provides their own specific parameters, described in dedicated pages of the documentation. Following, you find parameters valid for all of them.
</p>

<x-larastrap::title label="name" />

<p>
    The <x-larastrap::parameter>name</x-larastrap::parameter> parameter has two purposes: assign the relative HTML attribute to the input node, and access the same-named attribute of the undelying <x-larastrap::parameter>obj</x-larastrap::parameter> assigned to the parent <x-larastrap::element>x-larastrap::form</x-larastrap::element>.
</p>

@include('partials.example', ['snippet' => 'inputs.name'])

<x-larastrap::title label="nprefix / npostfix" />

<p>
    With <x-larastrap::parameter>nprefix</x-larastrap::parameter> and <x-larastrap::parameter>npostfix</x-larastrap::parameter> it is possible to prepend or append a string to the actual HTML "name" attribute of the node. This permits to decouple from the name of the <x-larastrap::parameter>obj</x-larastrap::parameter> attribute you want to use to populate the input field.
</p>
<p>
    Useful in particular to handle arrays of objects within a form.
</p>

@include('partials.example', ['snippet' => 'inputs.nfix'])

<x-larastrap::title label="textprepend / textappend" />

<p>
    When set with a value, the <x-larastrap::parameter>textprepend</x-larastrap::parameter> and <x-larastrap::parameter>textappend</x-larastrap::parameter> parameters enforces the creation of a <a href="https://getbootstrap.com/docs/5.3/forms/input-group/">Bootstrap's "input group"</a> and the addiction of an explicit label before or after the input field itself.
</p>

@include('partials.example', ['snippet' => 'inputs.textappend'])

<x-larastrap::title label="readonly / disabled" />

<p>
    The <x-larastrap::parameter>readonly</x-larastrap::parameter> and <x-larastrap::parameter>disabled</x-larastrap::parameter> parameters are both handled by default converting the value of the input field in plain text. If you want to preserve the classic behavior (an actual input field which is read-only or disabled), just use the <x-larastrap::parameter>asplaintext</x-larastrap::parameter> parameter (as any other parameter it can be defined once in your application's <a href="{{ route('docs.getting-started', ['#configure']) }}">global configuration</a>).
</p>

@include('partials.example', ['snippet' => 'inputs.readonly'])

@endsection
