@extends('layout.docs', [
    'title' => 'Forms | Larastrap',
    'claim' => 'How to use the Boostrap5 Forms component in Laravel',
])

@section('docs')

<h1>Forms</h1>

<p class="mt-4 lead">
    <x-larastrap::element>x-larastrap::form</x-larastrap::element> is one of the most complex (and useful) components in Larastrap. It plays well with <a href="{{ route('docs.field') }}">Field components</a> to provide a powerful tool to organize, populate and handle the forms of your Laravel application.
</p>

<x-larastrap::title label="obj" />

<p>
    As all other <a href="{{ route('docs.containers') }}">Containers</a>, forms have an <x-larastrap::parameter>obj</x-larastrap::parameter> parameter which can be populated with an object (eventually: an <a href="https://laravel.com/docs/eloquent">Eloquent model</a>). This parameter is inherited by all child <a href="{{ route('docs.input') }}">input fields</a>, which values are then inited with the proper attributes matched by their name.
</p>
<p>
    When <x-larastrap::parameter>obj</x-larastrap::parameter> is <x-larastrap::value>null</x-larastrap::value>, all child inputs are left with their explicit <x-larastrap::parameter>value</x-larastrap::parameter> (if set at all).
</p>

@include('partials.example', ['snippet' => 'forms.base'])

<p class="mt-4">
    When using <x-larastrap::parameter>obj</x-larastrap::parameter>, note that the <x-larastrap::parameter>name</x-larastrap::parameter> of the children Fields can be expressed in "dot notation" to access <a href="https://laravel.com/docs/eloquent-mutators#array-and-json-casting">arrays casted by the Eloquent model</a> or, anyway, the keys of the used associative array.
</p>

@include('partials.example', ['snippet' => 'forms.nested'])

<x-larastrap::title label="action / method" />

<p>
    <x-larastrap::parameter>action</x-larastrap::parameter> and <x-larastrap::parameter>method</x-larastrap::parameter> parameters are equivalent to the classic HTML attributes for forms. But you can specify once a default <x-larastrap::parameter>method</x-larastrap::parameter> in your configuration file (by default it is <x-larastrap::value>POST</x-larastrap::value>).
</p>
<p>
    Any <x-larastrap::parameter>method</x-larastrap::parameter> different than <x-larastrap::value>POST</x-larastrap::value> or <x-larastrap::value>GET</x-larastrap::value> translates to <code>@@method('XYZ')</code>, as <a href="https://laravel.com/docs/routing#form-method-spoofing">supported by Laravel</a>.
</p>

@include('partials.example', ['snippet' => 'forms.method'])

<x-larastrap::title label="route" />

<p>
    A convenient shortcut for <x-larastrap::parameter>action</x-larastrap::parameter> is <x-larastrap::parameter>route</x-larastrap::parameter>, where you specify just the name of the intended route which doesn't require addictional parameters.
</p>

@include('partials.example', ['snippet' => 'forms.route'])

<x-larastrap::title label="baseaction" />

<p>
    A bit more articulated is <x-larastrap::parameter>baseaction</x-larastrap::parameter>: this is intended to be the prefix of a <a href="https://laravel.com/docs/controllers#resource-controllers">Laravel's Resource Controller</a>.
</p>
<p>
    In the following example, when <x-larastrap::parameter>obj</x-larastrap::parameter> is set to <x-larastrap::value>NULL</x-larastrap::value> it is assigned as <x-larastrap::parameter>action</x-larastrap::parameter> the value <code>route('user.store')</code>; otherwise, if <x-larastrap::parameter>obj</x-larastrap::parameter> is a valid object, it will become <code>route('user.update', $obj->id)</code>. This is handy to have a single edit form to both create and update the same kind of model.
</p>
<p>
    Of course, when in update mode, the proper <x-larastrap::value>PUT</x-larastrap::value> <x-larastrap::parameter>method</x-larastrap::parameter> is used.
</p>

@include('partials.example', ['snippet' => 'forms.baseaction'])

<x-larastrap::title label="buttons" />

<p>
    The form's buttons (notably: the submit button) are generally not included within the <x-larastrap::element>x-larastrap::form</x-larastrap::element> node but are defined using the <x-larastrap::parameter>buttons</x-larastrap::parameter> parameter, which has to be an array of arrays, each rappresenting a button and with the parameters for the relevant <a href="{{ route('docs.button') }}">x-larastrap::button</a>.
</p>

@include('partials.example', ['snippet' => 'forms.buttons'])

<p class="mt-4">
    A peculiar parameter which can be used for those buttons is <x-larastrap::parameter>element</x-larastrap::parameter>, that permits to use a different node type for the element. By default it is of course <x-larastrap::value>larastrap::button</x-larastrap::value>, but can be any other Blade's Component identifier (remember to prefix Larastrap's widgets with <x-larastrap::value>larastrap::</x-larastrap::value>).
</p>

@include('partials.example', ['snippet' => 'forms.buttonselement'])

<p class="mt-4">
    Leveraging the <a href="{{ route('docs.text') }}">Typography</a> widget, you are actually able to append in the Form's footer any kind of text. Even better: any kind of text generated from the contextual <x-larastrap::parameter>obj</x-larastrap::parameter>.
</p>

@include('partials.example', ['snippet' => 'forms.buttonstelement'])

<x-larastrap::title label="buttons_align" />

<p>
    The <x-larastrap::parameter>buttons_align</x-larastrap::parameter> parameter permit to define where to put <x-larastrap::parameter>buttons</x-larastrap::parameter> within the form: by default their are aligned to <x-larastrap::value>end</x-larastrap::value>, but can also be at <x-larastrap::value>start</x-larastrap::value> or <x-larastrap::value>center</x-larastrap::value>.
</p>

@include('partials.example', ['snippet' => 'forms.buttonsalign'])

<x-larastrap::title label="formview" />

<p>
    It is possible to organize the layout of the Form in multiple ways, using the <x-larastrap::parameter>formview</x-larastrap::parameter> parameter. <x-larastrap::value>horizontal</x-larastrap::value> is the default value and behavior, but it is possible to opt for <x-larastrap::value>inline</x-larastrap::value>; it this case, please note that labels are converted in placeholders to accomodate into the row.
</p>

@include('partials.example', ['snippet' => 'forms.inline'])

<p class="mt-4">
    Similar, but not equal, is the <x-larastrap::value>squeeze</x-larastrap::value> alternative, which enforces the same named attribute on all children <a href="{{ route('docs.field') }}">Fields</a> and, as a conseguence, will omit the wrapping markup for each input element.
</p>
<p>
    This is in particular suggested when your form is mostly formatted into a table, and you want to omit explicit label for each single field.
</p>

@include('partials.example', ['snippet' => 'forms.squeeze'])

<p class="mt-4">
    Another - and more classical - option is the <x-larastrap::value>vertical</x-larastrap::value> value for <x-larastrap::parameter>formview</x-larastrap::parameter>.
</p>

@include('partials.example', ['snippet' => 'forms.vertical'])

<p class="mt-4">
    Anyway, you can use the <x-larastrap::parameter>formview</x-larastrap::parameter> attribute on each individual input component, to provide more complex layout.
</p>

@include('partials.example', ['snippet' => 'forms.mixedview'])

<x-larastrap::title label="Errors Handling" />

<p>
    Larastrap's forms have built-in integration with <a href="https://laravel.com/docs/validation">Laravel's Validation</a> and <a href="https://getbootstrap.com/docs/5.3/forms/validation/">Bootstrap's Validation</a>, and a few parameters to deal with it.
</p>
<p>
    By default <x-larastrap::parameter>error_handling</x-larastrap::parameter> is set to <x-larastrap::value>true</x-larastrap::value> and <x-larastrap::parameter>error_bag</x-larastrap::parameter> is set to <x-larastrap::value>default</x-larastrap::value>. When validation fails, and the error bag is populated, fields involved in the form but considered as valid are pre-populated with the previously submitted values through the <a href="https://laravel.com/docs/11.x/validation#repopulating-forms">proper Laravel helpers</a>.
</p>
</p>
    To ignore validation just set <x-larastrap::parameter>error_handling</x-larastrap::parameter> to <x-larastrap::value>false</x-larastrap::value>. To change <a href="https://laravel.com/docs/validation#named-error-bags">the name of the "error bag"</a> used by your server-side Laravel's validator, set <x-larastrap::parameter>error_bag</x-larastrap::parameter> accordingly.
</p>

<div class="alert alert-info">
    Submit the form below with at least an empty field to trigger server-side validation, then back here to see the result.
</div>

@include('partials.example', ['snippet' => 'forms.validation'])

<p class="mt-4">
    To, indeed, leverage <a href="https://getbootstrap.com/docs/5.3/forms/validation/">Bootstrap's client-side validation</a> functionality, use the <x-larastrap::parameter>client_side_errors</x-larastrap::parameter> parameter to deploy the relevant attributes in the HTML markup (and do not forget to init the proper Javascript code).
</p>

<div class="alert alert-info">
    Submit the form below with at least an empty field to trigger client-side validation.
</div>

@include('partials.example', ['snippet' => 'forms.validationnative'])

<p class="mt-4">
    Note that Larastrap can also handle <a href="https://laravel.com/docs/11.x/validation#validating-nested-array-input">nested array inputs</a> for more complex forms.
</p>

@include('partials.example', ['snippet' => 'forms.validationdot'])

@endsection
