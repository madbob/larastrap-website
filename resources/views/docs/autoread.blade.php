@extends('layout.docs', [
    'title' => 'Auto Read | Larastrap',
    'claim' => 'Automatically validate and read submitted forms in Laravel',
])

@section('docs')

<h1>Auto Read</h1>

<p class="mt-4 lead">
    Auto Read is a feature of <a href="{{ route('docs.forms') }}">Larastrap Forms</a> that makes it possible to validate and store informations with a single line of code in your Laravel Controllers.
</p>
<p>
    The essential idea is:
</p>
<ul>
    <li>building your forms in Blade using <a href="{{ route('docs.input') }}">Larastrap Input components</a>, it implicitely defines a semantic for the expected contents of the subsequent submission</li>
    <li>if Auto Read is enable, such data model is stored within the form itself as a JSON structured properly <a href="https://laravel.com/docs/encryption">encrypted</a> (to avoid malicious manipulation)</li>
    <li>when the form is submitted, the relative semantic model of the HTTP payload is retrieved and used to validate or to assign the new values to the proper Eloquent Model</li>
</ul>

<x-larastrap::title label="Enabling" />

<p>
    To enable Auto Read for any Larastrap Form, just add the boolean <x-larastrap::parameter>autoread</x-larastrap::parameter> parameter.
</p>
<p>
    This will append to the form itself a <x-larastrap::code>type="hidden"</x-larastrap::code> input with the full representation of each field and each attribute relevant for validation and interpretation of the submitted values.
</p>
<p>
    On submit, all fields in the Form will be processed to be assigned to the reference Model (or to a new instance, if none was assigned) accordingly to their name. If you want to exclude one or more fields, not intended to be reassigned to the Model (such as: an hidden input used for internal purposes), you can add them the <x-larastrap::parameter>skip_autoread</x-larastrap::parameter> parameter.
</p>

<x-larastrap::title label="Validation" />

<p>
    All server-side functions for Auto Read are implemented by the <x-larastrap::code>LarastrapStack</x-larastrap::code> singleton, registered by Larastrap's service provider.
</p>
<p>
    The <x-larastrap::code>autoreadValidate()</x-larastrap::code> method reads the incoming <a href="https://laravel.com/docs/requests">Request</a> and <a href="https://laravel.com/docs/validation">validates</a> it against the proper model previously generated when rendering the form.
</p>

<div class="alert alert-info">
    Submit the form below with at least an empty field to trigger server-side validation, then back here to see the result.
</div>

@include('partials.example', ['snippet' => 'autoread.validation'])

<p class="mt-4">
    When the second parameter of <x-larastrap::code>autoreadValidate()</x-larastrap::code> is set to <x-larastrap::value>true</x-larastrap::value>, classic validation behavior of Laravel is applied: in case of error, the proper error bag is filled with informations about errors and the user is redirected to the previous page.
</p>
<p>
    Otherwise, when set to <x-larastrap::value>false</x-larastrap::value>, the function just return <x-larastrap::value>true</x-larastrap::value> or <x-larastrap::value>false</x-larastrap::value> accordingly to the validation result, and permits you to proceed with your own business login.
</p>

<pre><code class="language-php">public function autoreadValidation(Request $request)
{
    $test = app()->make('LarastrapStack')->autoreadValidate($request, false);

    if ($test === false) {
        Log::debug('Invalid input!');
        return redirect()->route('home');
    }

    /* The rest of your Controller function */
}</code></pre>

<p>
    You can also use the <x-larastrap::code>autoreadValidationRules()</x-larastrap::code> function to retrieve the generated array of validation rules, to be extended with your own custom rules or just to be debugged.
</p>

<x-larastrap::title label="Storing" />

<p>
    The <x-larastrap::code>autoreadSave()</x-larastrap::code> function in <x-larastrap::code>LarastrapStack</x-larastrap::code> can be used to automatically transfer the data payload of the Request into the Eloquent Model assigned to the form.
</p>
<p>
    "Normal" inputs are just transferred by plain value, and "model" variants (as <a href="{{ route('docs.select') }}">Selects</a> and <a href="{{ route('docs.check-radio') }}">Checklists</a>) translate in Eloquent relations.
</p>
<p>
    Intentionally, <x-larastrap::code>autoreadSave()</x-larastrap::code> does not actually <x-larastrap::code>save()</x-larastrap::code> the model instance to permit you further elaboration.
</p>
<pre><code class="language-php">public function store(Request $request)
{
    app()->make('LarastrapStack')->autoreadValidate($request, true);

    /*
        It retrieves the Model assigned to the Form, or creates a new instance.
        Eventually you can use the same Controller method both to store or
        update Models of the same type
    */
    $user = app()->make('LarastrapStack')->autoreadSave($request, User::class);

    /*
        Do whatever you want before actual saving
    */
    $user->save();

    return redirect()->route('users.index');
}</code></pre>
<p>
    The Controller's method used in the following example is a bit more complex due his usage in this part of the documentation, but yet a few lines of code permit to manage different use cases.
</p>

@include('partials.example', ['snippet' => 'autoread.saving'])

<p class="mt-4">
    The optional second argument of <x-larastrap::code>autoreadSave()</x-larastrap::code> is a Model's class: it is used when the initial form is not built on an existing instance, but refers to a "create" action.
</p>

@include('partials.example', ['snippet' => 'autoread.creating'])

<p class="mt-4">
    The reference Model class intended to be saved may implement the <x-larastrap::code>AutoReadsFields</x-larastrap::code> interface for improved control over actual management of each submitted field. When this interface is used, each matching field retrieved in the incoming Request is first processed by your own implementation of the <x-larastrap::code>autoreadField()</x-larastrap::code> method and, only when it returns <x-larastrap::code>AutoReadOperation::Auto</x-larastrap::code>, the field is automatically managed.
</p>
<p>
    This is in particular required for <a href="{{ route('docs.file') }}">File</a> input types, as Larastrap does not makes any assumption on how and where to store uploaded files.
</p>

@include('partials.example', ['snippet' => 'autoread.file'])

<x-larastrap::title label="Testing" />

<p>
    When testing a Controller method based on <x-larastrap::code>LarastrapStack::autoreadSave()</x-larastrap::code>, it is important to note that the submitted form requires to contain his own autoread representation to be correctly handled. To easy this, it is provided the <x-larastrap::code>LarastrapStack::autoreadRender()</x-larastrap::code> function intended to render a template and obtain the relevant parts of the request to be submitted.
</p>

<pre><code class="language-php">$request = LarastrapStack::autoreadRender('my.template', $data_for_the_template);
$request = array_merge($request, $data_for_the_request);
$response = $this->post('/the/endpoint/for/the/rendered/form', $request);</code></pre>

@endsection
