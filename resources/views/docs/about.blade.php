@extends('layout.docs', [
    'title' => 'About | Larastrap',
    'claim' => 'Behind the scene of the Larastrap components library',
])

@section('docs')

<h1>About</h1>

<x-larastrap::title label="Team" />

<p>
    The maintainer of Larastrap is <a href="https://www.madbob.org/">Roberto Guido aka Bob</a>, a freelance software developer located in Turin, Italy.
</p>

<x-larastrap::title label="History" />

<p>
    Larastrap development started back in 2021 as a spin-off of <a href="https://www.gasdotto.net/">GASdotto</a>, an open source management platform for so-called "Ethical Purchasing Groups" (groups of people puchasing goods collectively from producers; <a href="https://en.wikipedia.org/wiki/Gruppi_di_Acquisto_Solidale">read more on Wikipedia</a>). As the core application included lots of custom widgets, chaotically scattered in Blade templates, it raised the need for better code organization and the desire to create something to be reused in other projects.
</p>
<p>
    During the first (unreleased) implementation, it become evident that with a few tuning it would have been possible to introduce the powerful concept of <a href="{{ route('docs.custom-elements') }}">Custom Elements</a> directly into the library and the whole core got rewritten to accomodate features for high customization and adaptability.
</p>
<p>
    Since then, Larastrap has been an essential building block for most of applications developed by the maintainer, both in personal projects and works for clients. This ensures a direct involvement in the evolution and maintainance of the Larastrap project, as it already is a dependency for many other platforms.
</p>

<img class="mt-3 img-fluid" src="{{ Vite::asset('resources/images/users.webp') }}">

<x-larastrap::title label="Get Involved" />

<p>
    Get involved with Larastrap development by <a href="https://gitlab.com/madbob/larastrap/-/issues">opening an issue</a> or submitting a pull request. Read our <a href="https://gitlab.com/madbob/larastrap/-/blob/master/CONTRIBUTING.md">contributing guidelines</a> for information on how we develop.
</p>

@endsection
