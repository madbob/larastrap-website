@extends('layout.docs', [
    'title' => 'Contribute | Larastrap',
    'claim' => 'Contribute to the Larastrap project',
])

@section('docs')

<h1>Contribute</h1>

<p class="mt-4 lead">
    Larastrap is an open source project, and you can help in fixing, extending and improving it.
</p>

<x-larastrap::title label="Setup" />

<p>
    To prepare your development environment, run
</p>

<pre><code class="language-php">git clone https://gitlab.com/madbob/larastrap.git
cd larastrap
composer install
</code></pre>

<p>
    It is possible to use your local (and, eventually, modified) version of the library on any locally installed Laravel application, for example by adding it temporarily to your <x-larastrap::code>composer.json</x-larastrap::code> file.
</p>

<pre><code class="language-php">{
    "repositories": [
        {
            "type": "path",
            "url": "/your/local/copy/of/larastrap"
        }
    ],
    "require": {
        "madbob/larastrap": "dev-master"
    }
}</code></pre>

<x-larastrap::title label="Tests" />

<p>
    To execute unit tests, run
</p>
<p>
    <x-larastrap::command value="vendor/bin/phpunit tests" />
</p>

<p>
    Please ensure that all tests pass before submitting your merge request, otherwise it will be blocked by automatic checks.
</p>

<x-larastrap::title label="Code Style" />

<p>
    The standard PHP code style is adopted. To automatically format the code you add or change, run
</p>
<p>
    <x-larastrap::command value="vendor/bin/php-cs-fixer fix src" />
</p>
<p>
    Please ensure to execute this command before to submit your merge request, otherwise it will be blocked by automatic checks.
</p>

<x-larastrap::title label="Documentation" />

<p>
    The code of this website is also available <a href="https://gitlab.com/madbob/larastrap-website">on GitLab</a> and can be improved with clarifications, examples, new tricky <a href="{{ route('docs.custom-examples') }}">custom elements</a> or just grammatical fixes.
</p>

<x-larastrap::title label="Love" />

<p>
    Do not forget to leave a Star on <a href="https://gitlab.com/madbob/larastrap/-/starrers">the GitLab repository</a>!
</p>
<p>
    Fun facts: the GitLab support on <a href="https://getcomposer.org/">Composer</a> and <a href="https://packagist.org/">Packagist</a> - the popular PHP package manager - has been added by the Larastrap's maintainer primarily to improve coverage for this library! See: <a href="https://github.com/composer/composer/pull/11734">this</a> and <a href="https://github.com/composer/packagist/pull/1408">this</a> pull requests!
</p>

<x-larastrap::title label="Donations" />

<p>
    To support the development, evolution and maintainance of Larastrap please consider to contribute with a donation.
</p>
<ul>
    <li>With <a href="https://www.paypal.com/donate?hosted_button_id=GRKVVL3E5KZ4Q">PayPal</a></li>
    <li>With <a href="https://github.com/sponsors/madbob">GitHub Sponsors</a> (even if Larastrap repositories are on GitLab...)</li>
</ul>
<p>
    Larastrap's maintainer invites all professional developers using open source software within their daily job (desktop or web applications, modules, libraries...) to distribute 1% of their annual revenues to those same open projects from which the revenues themselves have been generated.
</p>
<p>
    To easily retrieve informations about projects accepting donations you can use
</p>
<ul>
    <li><a href="https://getcomposer.org/doc/03-cli.md#fund">composer fund</a> for PHP packages</li>
    <li><a href="https://docs.npmjs.com/cli/v10/commands/npm-fund">npm fund</a> for Javascript packages</li>
    <li><a href="https://apt.gives/">apt-give</a> for Debian/Ubuntu packages</li>
</ul>

@endsection
