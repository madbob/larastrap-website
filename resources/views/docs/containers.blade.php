@extends('layout.docs', [
    'title' => 'Containers | Larastrap',
    'claim' => 'How to use the Boostrap5 Containers component in Laravel',
])

@section('docs')

<h1>Containers</h1>

<p class="mt-4 lead">
    Larastrap provides many "container" components: those having a child HTML block to be wrapped within the container itself. All of them share a few parameters.
</p>

<x-larastrap::title label="obj" />

<p>
    <x-larastrap::parameter>obj</x-larastrap::parameter> is used in particular for <x-larastrap::element>x-larastrap::form</x-larastrap::element>, but can be used in any Container to define a source for values in child nodes.
</p>

@include('partials.example', ['snippet' => 'containers.obj'])

<x-larastrap::title label="prependNodes / appendNodes" />

<p>
    All Containers have parameters <x-larastrap::parameter>prependNodes</x-larastrap::parameter> and <x-larastrap::parameter>appendNodes</x-larastrap::parameter> which can be populated with an array of plain custom HTML strings or with more arrays, each rappresenting a Larastrap node (with the same syntax adopted for Custom Elements).
</p>
<p>
    Those are useful to define complex <a href="{{ route('docs.custom-elements') }}">Custom Elements</a> with once-implemented design, to be then reused multiple times in your own templates.
</p>

@include('partials.custom', ['snippet' => 'customs.infoform'])

<p class="mt-4">
    Note the difference among <x-larastrap::parameter>prependNodes</x-larastrap::parameter>/<x-larastrap::parameter>appendNodes</x-larastrap::parameter> parameters in Containers and <x-larastrap::parameter>innerPrependNodes</x-larastrap::parameter>/<x-larastrap::parameter>innerAppendNodes</x-larastrap::parameter> parameters: the latter are specific for <a href="{{ route('docs.text') }}">Typography nodes</a>, and intended for even more complex layouts and hierarchies of HTML nodes.
</p>

@endsection
