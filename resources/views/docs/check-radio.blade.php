@extends('layout.docs', [
    'title' => 'Checks and Radios | Larastrap',
    'claim' => 'How to use the Boostrap5 Checks and Radios components in Laravel',
])

@section('docs')

<h1>Checks and Radios</h1>

<p>
    <x-larastrap::element>x-larastrap::check</x-larastrap::element> is used for a single checkbox, but many other variants are available.
</p>

@include('partials.example', ['snippet' => 'checks.single'])

<x-larastrap::title label="switch" />

<p>
    Adding the <x-larastrap::parameter>switch</x-larastrap::parameter> parameter to a single <x-larastrap::element>x-larastrap::check</x-larastrap::element> element, it will be presented as a <a href="https://getbootstrap.com/docs/5.3/forms/checks-radios/#switches">Bootstrap switch</a>.
</p>

@include('partials.example', ['snippet' => 'checks.switch'])

<x-larastrap::title label="inline" />

<p>
    The <x-larastrap::parameter>inline</x-larastrap::parameter> parameter is useful to alter the layout of the checkbox component.
</p>
<p>
    By default <x-larastrap::element>x-larastrap::check</x-larastrap::element> is handled as any other <a href="{{ route('docs.input') }}">Input element</a> (the position of label and input are determined by the parent <a href="{{ route('docs.forms') }}">Form</a> <x-larastrap::parameter>formview</x-larastrap::parameter>), while <x-larastrap::parameter>inline</x-larastrap::parameter> groups the label with the actual checkbox for a more classic visualization.
</p>

@include('partials.example', ['snippet' => 'checks.inline'])

<p class="mt-4">
    As per any other Larastrap parameter, for all elements, if you want to use the <x-larastrap::parameter>inline</x-larastrap::parameter> layout by default you can enforce it once in your <a href="{{ route('docs.getting-started') }}">configuration file</a>.
</p>

<pre><code class="language-php">&lt;?php

return [
    'elements' => [
        'check' => [
            'inline' => true,
        ],
    ],
];</code></pre>

<x-larastrap::title label="options" />

<p>
    <x-larastrap::element>x-larastrap::checks</x-larastrap::element> and <x-larastrap::element>x-larastrap::radios</x-larastrap::element> act as a single input field, but are actually a collection of organized items. Each item is an "option", enumerated into the <x-larastrap::parameter>options</x-larastrap::parameter> parameter.
</p>

@include('partials.example', ['snippet' => 'checks.base'])

<p class="mt-4">
    <x-larastrap::parameter>options</x-larastrap::parameter> can be a simple associative array where keys are the HTML values and values are the related labels. But each value in the options can also be an object, holding a few attributes to better describe each item. Among them:
</p>
<ul>
    <li><x-larastrap::parameter>label</x-larastrap::parameter> - the label to display</li>
    <li><x-larastrap::parameter>label_html</x-larastrap::parameter> - alternative to label, to enforce an HTML content</li>
    <li><x-larastrap::parameter>tlabel</x-larastrap::parameter> - alternative to label, contains an identifier for native <a href="{{ route('docs.translations') }}">translations</a></li>
    <li><x-larastrap::parameter>id</x-larastrap::parameter> - an optional ID to identify the option in the DOM. Please note: if you specify an ID, it is up to you to grant it is unique within the current page</li>
    <li><x-larastrap::parameter>disabled</x-larastrap::parameter> - set true if the option is not selectable</li>
    <li><x-larastrap::parameter>hidden</x-larastrap::parameter> - set true to skip rendering of the label but still keep the option within the page. This can be useful to handle some extra option through Javascript</li>
    <li><x-larastrap::parameter>classes</x-larastrap::parameter> - CSS classes to be added to the button</li>
    <li><x-larastrap::parameter>attributes</x-larastrap::parameter> - associative array of HTML attributes to be assigned to the button</li>
</ul>

@include('partials.example', ['snippet' => 'checks.options'])

<x-larastrap::title label="As Lists" />

<p>
    <x-larastrap::element>x-larastrap::checklist</x-larastrap::element> and <x-larastrap::element>x-larastrap::radiolist</x-larastrap::element> are alternative components, where options are rappresented as horizontal rows instead of a <a href="{{ route('docs.button-group') }}"><x-larastrap::element>x-larastrap::btngroup</x-larastrap::element></a>.
</p>

@include('partials.example', ['snippet' => 'checks.lists'])

<p class="mt-4">
    Of course, this layout also handles the same optional customizations in the <x-larastrap::parameter>options</x-larastrap::parameter> array.
</p>

@include('partials.example', ['snippet' => 'checks.options_lists'])

<x-larastrap::title label="With Models" />

<p>
    Other variants permit to use objects (eventually: <a href="https://laravel.com/docs/eloquent">Eloquent models</a>) as options within a checks list or radio buttons set. Those are
</p>
<ul>
    <li><x-larastrap::element>x-larastrap::checks-model</x-larastrap::element></li>
    <li><x-larastrap::element>x-larastrap::radios-model</x-larastrap::element></li>
    <li><x-larastrap::element>x-larastrap::checklist-model</x-larastrap::element></li>
    <li><x-larastrap::element>x-larastrap::radiolist-model</x-larastrap::element></li>
</ul>

@include('partials.example', ['snippet' => 'checks.models'])

<p class="mt-4">
    The <x-larastrap::code>-model</x-larastrap::code> variants are able to set their own value accordingly to the <a href="https://laravel.com/docs/eloquent-relationships">Eloquent relationships</a> of the reference entity (the one assigned as <x-larastrap::parameter>obj</x-larastrap::parameter> of the parent <x-larastrap::element>x-larastrap::form</x-larastrap::element>), as long as their <x-larastrap::parameter>name</x-larastrap::parameter> matches the attribute to which the relationship itself is exposed.
</p>

@include('partials.example', ['snippet' => 'checks.modelsselection'])

<p class="mt-4">
    For <x-larastrap::code>-model</x-larastrap::code> variants it is assumed that models into the <x-larastrap::parameter>options</x-larastrap::parameter> collection have an "id" and a "name" attribute to be used (respectively: as the value and the label of each item).
</p>
<p>
    If your models have a different structure, or you want to display different labels, it is possible to pass a <x-larastrap::parameter>translateCallback</x-larastrap::parameter> parameter with a function to translate your actual content: it has to accept a parameter (the model to be translated) and must return an array with two elements (the value and the label of the final selectable option).
</p>

@include('partials.example', ['snippet' => 'checks.modelscallback'])

<p class="mt-4">
    The <x-larastrap::code>-model</x-larastrap::code> variants have also an <x-larastrap::parameter>extra_options</x-larastrap::parameter> parameter to define other options aside those collected in the <x-larastrap::parameter>options</x-larastrap::parameter>. This is useful to add a "void" item to be choosen by the user.
</p>

@include('partials.example', ['snippet' => 'checks.modelsextra'])

@endsection
