@extends('layout.docs', [
    'title' => 'Enclose | Larastrap',
    'claim' => 'How to use the Boostrap5 Enclose component in Laravel',
])

@section('docs')

<h1>Enclose</h1>

<p class="mt-4">
    Enclose is a peculiar type of element provided by Larastrap: similar to a <a href="{{ route('docs.forms') }}">Form</a>, it acts as a logical container for other elements without providing any actual HTML markup.
</p>

@include('partials.example', ['snippet' => 'encloses.base'])

<p class="mt-4">
    His may purpose is to isolate different models within the same logical block. The common use case is: a form with a table involving multiple models at once, each with his own attributes (and his own input fields).
</p>

@include('partials.example', ['snippet' => 'encloses.table'])

<p class="mt-4">
    In the example above, it is important to note the use of <a href="{{ route('docs.input', '#nprefix-npostfix') }}"><x-larastrap::parameter>npostfix</x-larastrap::parameter></a> parameter: when the form is submitted, all email addresses have to be serialized into an array and sent to the specified endpoint (the form's <x-larastrap::parameter>action</x-larastrap::parameter>).
</p>

<p>
    Enclose can also be used as a generic container in <a href="{{ route('docs.custom-elements') }}">Custom Elements</a>, to build your own composite widgets leveraging properties <x-larastrap::parameter>prependNodes</x-larastrap::parameter> and <x-larastrap::parameter>appendNodes</x-larastrap::parameter> common to all <a href="{{ route('docs.containers') }}">Containers</a>.
</p>
<p>
    The following example is quite silly, but demostrates how it is possible to pack a button and an hidden input field in a single-line widget to be reused across the whole application.
</p>

@include('partials.custom', ['snippet' => 'customs.composite'])

<x-larastrap::title label="nprefix / npostfix" />

<p>
    For convenience, it is possible to set both <x-larastrap::parameter>nprefix</x-larastrap::parameter> and <x-larastrap::parameter>npostfix</x-larastrap::parameter> parameters once on the Enclose, to be inherited by all the child <a href="{{ route('docs.input') }}">Inputs</a>.
</p>

@include('partials.example', ['snippet' => 'encloses.nprefix'])

@endsection
