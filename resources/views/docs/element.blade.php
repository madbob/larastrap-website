@extends('layout.docs', [
    'title' => 'Larastrap: Base Element',
    'claim' => 'Documentation about the base Elements in Larastrap',
])

@section('docs')

<h1>Base Element</h1>

<p class="lead">
    All components implemented in Larastrap share the same basic class, and the same essential parameters.
</p>

<x-larastrap::title label="id" />

<p>
    The ID of the HTML node. If not specified, a (semi) random one is generated so that all items are properly marked.
</p>

@include('partials.example', ['snippet' => 'base.id'])

<x-larastrap::title label="attributes" />

<p>
    An associative array which is translated to an extra set of attributes to the node. Useful to attach your own data attributes, custom HTML attributes, hooks for your preferred Javascript framework and so on.
</p>

@include('partials.example', ['snippet' => 'base.attributes'])

<p class="mt-4">
    Actually, the <x-larastrap::parameter>attributes</x-larastrap::parameter> parameter is mostly intended to easy algorithmic definition and generation of addictional attributes, as any attribute passed to the Blade node and not processed by Larastrap will be anyway appended to the final HTML output.
</p>

<p>
    Prepend a colon ( <x-larastrap::value>:</x-larastrap::value> ) to the name of the attribute to pass a PHP expression to be evaluated, as described <a href="https://laravel.com/docs/blade#passing-data-to-components">in the Laravel documentation</a>.
</p>

@include('partials.example', ['snippet' => 'base.anyattribute'])

<x-larastrap::title label="classes" />

<p>
    An array of CSS classes to be added to the HTML node, in addiction to those automatically generated. Can be defined as an array or a space separated string.
</p>

@include('partials.example', ['snippet' => 'base.classes'])

<x-larastrap::title label="override_classes" />

<p>
    An array of CSS classes to be applied to the HTML node, overriding those automatically generated. Can be defined as an array or a space separated string.
</p>

@include('partials.example', ['snippet' => 'base.override_classes'])

<x-larastrap::title label="hidden" />

<p>
    A boolean attribute which hides the element.
</p>

@include('partials.example', ['snippet' => 'base.hidden'])

<x-larastrap::title label="reviewCallback" />

<p>
    A function to be called to directly handle and transform the parameters of the node. This have to take two parameters: the first is the instance of the component itself, the second is the indexed array of parameters. Must return the (eventually modified) array of parameters.
</p>
<p>
    This feature is mostly intended for <a href="{{ route('docs.custom-elements') }}">Custom Elements</a>, and is the ultimate customization tool provided by Larastrap.
</p>

@include('partials.example', ['snippet' => 'base.review'])

<p class="mt-4">
    The <x-larastrap::parameter>reviewCallback</x-larastrap::parameter> can also be indicated in any form accepted as <a href="https://www.php.net/manual/en/language.types.callable.php">PHP Callable</a>, such as <code>[MyClass::class, 'a_static_method']</code>: this is in particular suggested when using this parameter in your <a href="{{ route('docs.getting-started', '#configure') }}">configuration file</a>, to be able to cache it using <a href="https://laravel.com/docs/11.x/configuration#configuration-caching">the dedicated Laravel command</a>.
</p>

@include('partials.example', ['snippet' => 'base.reviewarray'])

@endsection
