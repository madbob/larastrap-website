@extends('layout.docs', [
    'title' => 'Custom Elements Examples | Larastrap',
    'claim' => 'How to use Boostrap5 Custom Elements in Laravel',
])

@section('docs')

<h1>Custom Elements Examples</h1>

<p class="mt-4 lead">
    Larastrap provides the ability to define your own custom element, just extending the existing ones and providing some configuration. Those can be paired with Javascript libraries and custom PHP functions to fine-tune the intended behaviour, and reuse across your code.
</p>

@foreach($recipes as $recipe)
    <x-larastrap::title :label="$recipe->folder" />

    <p>
        {!! nl2br($recipe->description) !!}
    </p>

    @include('partials.custom', ['snippet' => 'recipes.' . $recipe->folder])
@endforeach

@endsection
