@extends('layout.docs', [
    'title' => 'Collapses | Larastrap',
    'claim' => 'How to use the Boostrap5 Collapse component in Laravel',
])

@section('docs')

<h1>Collapses</h1>

<p>
    Usually you may want a trigger element to open or close a collapsible panel: apply the <a href="{{ route('docs.button', '#triggers-collapse-triggers-modal') }}"><x-larastrap::parameter>triggers_collapse</x-larastrap::parameter></a> parameter to a <a href="{{ route('docs.button') }}">Button</a> to link that node to a <x-larastrap::element>x-larastrap::collapse</x-larastrap::element>.
</p>

@include('partials.example', ['snippet' => 'collapses.base'])

<p class="mt-4">
    Applying <x-larastrap::parameter>triggers_collapse</x-larastrap::parameter> to a <x-larastrap::element>x-larastrap::check</x-larastrap::element>, the initial open/close status of <x-larastrap::element>x-larastrap::collapse</x-larastrap::element> is determined by the checked status of the checkbox itself.
</p>

@include('partials.example', ['snippet' => 'collapses.check'])

<x-larastrap::title label="open" />

<p>
    Alternatively, you can enforce the open/close status of the panel using the <x-larastrap::parameter>open</x-larastrap::parameter> parameter to it.
</p>

@include('partials.example', ['snippet' => 'collapses.open'])

@endsection
