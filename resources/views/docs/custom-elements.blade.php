@extends('layout.docs', [
    'title' => 'Custom Elements | Larastrap',
    'claim' => 'How to use Boostrap5 Custom Elements in Laravel',
])

@section('docs')

<h1>Custom Elements</h1>

<p class="mt-4">
    Given the multiple kind of components provided by Larastrap, it is possible to define your owns. Once, with a few lines in <a href="{{ route('docs.getting-started', '#configure') }}">the configuration</a>, and ready to be used across all of your application.
</p>

<p>
    Custom Elements can be defined in the <x-larastrap::code>customs</x-larastrap::code> array in <x-larastrap::code>config/larastrap.php</x-larastrap::code>, in the form
</p>

<pre><code class="language-php">&lt;?php

return [
    'customs' => [
        'my-element-name' => [
            'extends' => 'a-larastrap-kind-of-element',
            'params' => [
                'first_parameter' => 'value',
                'second_parameter' => 'value',
            ]
        ]
    ]
];</code></pre>

<p>
    or, for a more practical example
</p>

<pre><code class="language-php">&lt;?php

return [
    'customs' => [
        'mybutton' => [
            'extends' => 'button',
            'params' => [
                'color' => 'warning',
            ]
        ]
    ]
];</code></pre>

<p>
    Given this configuration, every time you use <x-larastrap::element>x-larastrap::mybutton</x-larastrap::element> tag, you obtain a <a href="{{ route('docs.button') }}"><x-larastrap::element>x-larastrap::button</x-larastrap::element></a> whose default <a href="{{ route('docs.button', '#label-color') }}"><x-larastrap::parameter>color</x-larastrap::parameter></a> is <x-larastrap::value>warning</x-larastrap::value>
</p>

@include('partials.example', ['snippet' => 'customs.base'])

<p class="mt-4">
    The most immediate use of this feature is the ability to provide a consistent semantic for your application, for example defining once the buttons to create, modify, delete items and specifing in a single place their graphical behavior (attaching specific CSS classes or using the Bootstrap one's).
</p>

<p>
    Then, combining advanced features as <a href="{{ route('docs.text') }}">Typography</a> components, <x-larastrap::element>prependNodes</x-larastrap::element>/<x-larastrap::element>appendNodes</x-larastrap::element> for <a href="{{ route('docs.containers') }}">Containers</a> and a bit of creativity, you are able to built complex composite widgets (often, faster and better than using <a href="https://laravel.com/docs/blade#components">Laravel Blade native Components</a>) to be also shared in different applications.
</p>

<x-larastrap::title label="Dynamic Load" />

<p>
    You can also dynamically define your custom elements with the <code>addCustomElement()</code> provided by <a href="{{ route('docs.getting-started', '#the-stack') }}">LarastrapStack</a> service provider. This has to be used before Blade rendering, for example in the parent Controller or in a <a href="https://laravel.com/docs/providers">Laravel Service Provider</a>.
</p>

<code><pre class="language-php">app()->make('LarastrapStack')->addCustomElement('jerkbutton', [
    'extends' => 'button',
    'params' => [
        'color' => 'secondary',
        'postlabel' => ' (Click Here!)',
    ],
]);</code></pre>

@include('partials.example', ['snippet' => 'customs.dynamic'])

@endsection
