@extends('layout.docs', [
    'title' => 'Upgrade | Larastrap',
    'claim' => 'Guidelines to upgrade from different Larastrap versions',
])

@section('docs')

<h1>Upgrade</h1>

<x-larastrap::title label="From 0 to 1" />

<p>
    Some attributes in the <x-larastrap::parameter>options</x-larastrap::parameter> parameter of <a href="{{ route('docs.check-radio') }}">checks and radios components</a> have been renamed, to match naming used in other contexts.
</p>
<ul>
    <li><x-larastrap::parameter>button_classes</x-larastrap::parameter> has been renamed to <x-larastrap::parameter>classes</x-larastrap::parameter></li>
    <li><x-larastrap::parameter>button_attributes</x-larastrap::parameter> has been renamed to <x-larastrap::parameter>attributes</x-larastrap::parameter></li>
</ul>

<hr>

<p>
    Previously undocumented <x-larastrap::parameter>appendNodes</x-larastrap::parameter> parameter in <a href="{{ route('docs.containers') }}">Containers</a> have changed syntax, to match syntax already used for <a href="{{ route('docs.custom-elements') }}">Custom Elements</a>.
</p>
<div class="row g-2">
    <div class="col">
        <div class="card">
            <div class="card-header">Before</div>
            <div class="card-body">
                <pre class="m-0"><code>[
    'appendNodes' => [
        't' => [
            'node' => 'span',
            'classes' => ['text-success']
        ]
    ]
]</code></pre>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card">
            <div class="card-header">After</div>
            <div class="card-body">
                <pre class="m-0"><code>[
    'appendNodes' => [
        [
            'extends' => 't',
            'params' => [
                'node' => 'span',
                'classes' => ['text-success']
            ]
        ]
    ]
]</code></pre>
            </div>
        </div>
    </div>
</div>

@endsection
