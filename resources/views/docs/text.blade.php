@extends('layout.docs', [
    'title' => 'Typography | Larastrap',
    'claim' => 'How to use Boostrap5 Typography in Laravel',
])

@section('docs')

<h1>Typography</h1>

<p class="mt-4">
    Larastrap provides a generic utility component called <x-larastrap::element>x-larastrap::t</x-larastrap::element>, able to be rendered as any preferred HTML tag type.
</p>

@include('partials.example', ['snippet' => 'typography.base'])

<p class="mt-4">
    Of course this is not meant to be directly used in templates - as it would be tedious and unconvenient to write so much code just to open and close a <x-larastrap::code>span</x-larastrap::code> - but is considered a base to define your own <a href="{{ route('docs.custom-elements') }}">Custom Elements</a>, so to define the typography style of your whole application and keep it consistent through the centralized configuration.
</p>

@include('partials.custom', ['snippet' => 'customs.typography'])

<p class="mt-4">
    <x-larastrap::element>x-larastrap::t</x-larastrap::element> can be used to handle, among the other, classic Bootstrap Alerts and Badges: get a look to the page of <a href="{{ route('docs.custom-examples') }}">Custom Elements Examples</a>.
</p>

<x-larastrap::title label="content" />

<p>
    When you just want a shortcut for a static label, you can use the <x-larastrap::parameter>content</x-larastrap::parameter> parameter to define once a string to be placed into the node.
</p>

@include('partials.custom', ['snippet' => 'customs.statusbadges'])

<x-larastrap::title label="obj / name" />

<p>
    <x-larastrap::element>x-larastrap::t</x-larastrap::element> is a <a href="{{ route('docs.containers') }}">Container</a>, and an <x-larastrap::parameter>obj</x-larastrap::parameter> can be assigned (or inherited), but it also sports a few other parameters that permits you to display the value from a named attribute of the object (eventually: an <a href="https://laravel.com/docs/eloquent">Eloquent model</a>) directly within the textual node
</p>
<ul>
    <li><x-larastrap::parameter>name</x-larastrap::parameter> - same as <a href="{{ route('docs.input') }}">in Inputs</a>, define the name of the attribute to access</li>
    <li><x-larastrap::parameter>prelabel / postlabel</x-larastrap::parameter> - same as <a href="{{ route('docs.button', '#prelabel-postlabel') }}">in Buttons</a>, permit to prepend or append an arbitrary text</li>
</ul>

@include('partials.example', ['snippet' => 'typography.obj'])

<p class="mt-4">
    In this case, also, it is possible to define your own <a href="{{ route('docs.custom-elements') }}">Custom Elements</a> to always display the same attribute with the same style, decorations, addictional graphics and more.
</p>

@include('partials.custom', ['snippet' => 'customs.card'])

<x-larastrap::title label="inner prepend / append" />

<p>
    As <x-larastrap::element>x-larastrap::t</x-larastrap::element> is a Container, it supports <x-larastrap::parameter>prependNodes</x-larastrap::parameter>/<x-larastrap::parameter>appendNodes</x-larastrap::parameter> parameters to attach items before and after the element itself. But the Typography node also has <x-larastrap::parameter>innerPrependNodes</x-larastrap::parameter>/<x-larastrap::parameter>innerAppendNodes</x-larastrap::parameter> parameters to be able to prepend and append items inside the element, and be able to define once complex layouts.
</p>
<p>
    The above example of users' card can be squeezed into a single Larastrap Custom Element containing multiple informations.
</p>

@include('partials.custom', ['snippet' => 'customs.squeezedcard'])

@endsection
