@extends('layout.docs', [
    'title' => 'Changelog | Larastrap',
    'claim' => 'Recent changes and improvements in Larastrap',
])

@section('docs')

<div class="changelog">
    @php
    $converter = new \League\CommonMark\CommonMarkConverter();
    echo $converter->convert(file_get_contents(base_path('vendor/madbob/larastrap/CHANGELOG.md')));
    @endphp
</div>

@endsection
