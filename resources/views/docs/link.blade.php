@extends('layout.docs', [
    'title' => 'Links | Larastrap',
    'claim' => 'The Link component in Larastrap',
])

@section('docs')

<h1>Links</h1>

<p class="mt-4 lead">
    Links behave as <a href="{{ route('docs.button') }}">Buttons</a> (and have all of their parameters), but generate <x-larastrap::code>&lt;a></x-larastrap::code> HTML nodes.
</p>

<x-larastrap::title label="href" />

<p>
    <x-larastrap::parameter>href</x-larastrap::parameter> is serialized, obviously, as the actual URL to be linked.
</p>

@include('partials.example', ['snippet' => 'links.base'])

<x-larastrap::title label="route" />

<p>
    When linking to a named plain Laravel route with no arguments, you can just use the <x-larastrap::parameter>route</x-larastrap::parameter> shortcut and specify only the route's name.
</p>

@include('partials.example', ['snippet' => 'links.route'])

@endsection
