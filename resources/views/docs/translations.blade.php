@extends('layout.docs', [
    'title' => 'Translations | Larastrap',
    'claim' => 'How to use Translatable components in Laravel',
])

@section('docs')

<h1>Translations</h1>

<p class="mt-4">
    Many parameters provide a translatable alternative: when populated with an identifier for the native <a href="https://laravel.com/docs/localization">Laravel's localization ability</a>, those assign a translated string to the reference attribute.
</p>

@include('partials.example', ['snippet' => 'translations.base'])

@php

$data = [
    'button' => [
        'attributes' => [
            'tlabel' => 'label',
            'ttitle' => 'title',
        ],
    ],
    'field' => [
        'intro' => 'Those attributes are inherited by all <a href="' . route('docs.input') . '">Inputs</a> and other components usually used in <a href="' . route('docs.forms') . '">Forms</a>.',
        'example' => 'translations.field',
        'attributes' => [
            'tlabel' => 'label',
            'thelp' => 'help',
            'tpophelp' => 'pophelp',
        ],
    ],
    'input' => [
        'attributes' => [
            'tplaceholder' => 'placeholder',
            'ttextprepend' => 'textprepend',
            'ttextappend' => 'textappend',
        ],
    ],
    'checks, radios, select' => [
        'intro' => 'Within the "options" parameter of <a href="' . route('docs.select') . '">Select</a>, <a href="' . route('docs.check-radio') . '">Checks</a> and other similar widgets, it is possible to use dedicated attributes for strings to be translated.',
        'attributes' => [
            'options.tlabel' => 'options.label',
        ],
    ],
    'pophelp' => [
        'attributes' => [
            'ttext' => 'text',
        ],
    ],
    'accordionitem' => [
        'attributes' => [
            'tlabel' => 'label',
        ],
    ],
    'tabpane' => [
        'attributes' => [
            'tlabel' => 'label',
        ],
    ],
];

@endphp

@foreach($data as $class => $attributes)
    <x-larastrap::title :label="$class" />

    @if(isset($attributes['intro']))
        <p>
            {!! $attributes['intro'] !!}
        </p>
    @endif

    <ul>
        @foreach($attributes['attributes'] as $name => $ref)
            <li><x-larastrap::parameter>{{ $name }}</x-larastrap::parameter> translates <x-larastrap::parameter>{{ $ref }}</x-larastrap::parameter></li>
        @endforeach
    </ul>

    @if(isset($attributes['example']))
        @include('partials.example', ['snippet' => $attributes['example']])
    @endif
@endforeach

@endsection
