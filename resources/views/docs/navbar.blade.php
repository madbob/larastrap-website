@extends('layout.docs', [
    'title' => 'Navbar | Larastrap',
    'claim' => 'How to use the Boostrap5 Navbar component in Laravel',
])

@section('docs')

<h1>Navbar</h1>

<x-larastrap::title label="options" />

<p>
    The essential parameter for <x-larastrap::element>x-larastrap::navbar</x-larastrap::element> is <x-larastrap::parameter>options</x-larastrap::parameter>: an associative array with items to be placed within the navbar itself. In this array, keys are the labels for the menu and values are the related URLs to be linked.
</p>

@include('partials.example', ['snippet' => 'navbar.base'])

<p class="mt-4">
    Each value in the <x-larastrap::parameter>options</x-larastrap::parameter> can be an associative array itself, holding a few attributes to better describe each item. Among them:
</p>
<ul>
    <li><x-larastrap::code>url</x-larastrap::code> - the URL linked by the item; alternative to <x-larastrap::code>route</x-larastrap::code></li>
    <li><x-larastrap::code>route</x-larastrap::code> - the name of the <a href="https://laravel.com/docs/routing#named-routes">route</a> linked by the item; alternative to <x-larastrap::code>url</x-larastrap::code></li>
    <li><x-larastrap::code>active</x-larastrap::code> - to enforce the given item to be marked as active</li>
    <li><x-larastrap::code>attributes</x-larastrap::code> - an associative array with arbitrary HTML attributes to be assigned to the item</li>
    <li><x-larastrap::code>children</x-larastrap::code> - for addictional sub-items, see the <a href="#sub-menus">dedicated paragraph</a> for details</li>
</ul>

@include('partials.example', ['snippet' => 'navbar.complex'])

<p class="mt-4">
    The <x-larastrap::parameter>active</x-larastrap::parameter> is optional: if Larastrap identifies the current path (or route) as the target for an item in the navbar, it automatically set it in the active state.
</p>

@include('partials.example', ['snippet' => 'navbar.current'])

<p class="mt-4">
    It is available also a <x-larastrap::parameter>end_options</x-larastrap::parameter> parameter, with the same contents of <x-larastrap::parameter>options</x-larastrap::parameter> but able to align at the end of the navbar more items.
</p>

@include('partials.example', ['snippet' => 'navbar.end'])

<x-larastrap::title label="collapse" />

<p>
    A core function of native Bootstrap's navbar is his "responsive behavior", able to reshape the whole menu to be nicely accessible on mobile. By default, Larastrap's navbar collapses the navbar at <x-larastrap::value>lg</x-larastrap::value> breakpoint, but it is possible to specify the preferred one (or do not collapse it at all, using the value <x-larastrap::value>never</x-larastrap::value>).
</p>
<p>
    Resize this page to see the different behaviors with different values for the <x-larastrap::parameter>collapse</x-larastrap::parameter> parameter.
</p>

@include('partials.example', ['snippet' => 'navbar.collapse'])

<x-larastrap::title label="title" />

<p>
    A <x-larastrap::element>x-larastrap::navbar</x-larastrap::element> may also have a title, and a related link associated.
</p>

@include('partials.example', ['snippet' => 'navbar.title'])

<x-larastrap::title label="color" />

<p>
    The <x-larastrap::parameter>color</x-larastrap::parameter> parameter is a shortcut to add <x-larastrap::code>navbar-light</x-larastrap::code> or <x-larastrap::code>navbar-dark</x-larastrap::code> classes to the navbar; remember you can always use the <a href="{{ route('docs.element', '#classes') }}"><x-larastrap::parameter>classes</x-larastrap::parameter> base parameter</a> to use all CSS classes you want.
</p>

@include('partials.example', ['snippet' => 'navbar.color'])

<x-larastrap::title label="container" />

<p>
    The default wrapping element of a <x-larastrap::element>x-larastrap::navbar</x-larastrap::element> is Bootstrap's <x-larastrap::value>.container-fluid</x-larastrap::value>, which expands the bar across the whole screen width, but you may want to just use <x-larastrap::value>.container</x-larastrap::value> to shrink his width.
</p>
<p>
    In this case, use the <x-larastrap::parameter>container</x-larastrap::parameter> parameter.
</p>

@include('partials.example', ['snippet' => 'navbar.container'])

<x-larastrap::title label="Sub Menus" />

<p>
    It is possible to define sub-menus using a <x-larastrap::code>children</x-larastrap::code> index in each option. It must be an associative array, following the same rules of <x-larastrap::parameter>options</x-larastrap::parameter>.
</p>

@include('partials.example', ['snippet' => 'navbar.children'])

@endsection
