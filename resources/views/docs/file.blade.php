@extends('layout.docs', [
    'title' => 'File Inputs | Larastrap',
    'claim' => 'How to use the Boostrap5 Input File component in Laravel',
])

@section('docs')

<h1>File Inputs</h1>

<p>
    <x-larastrap::element>x-larastrap::file</x-larastrap::element> is a <a href="{{ route('docs.input') }}">Input</a> component with a few peculiar behaviors.
</p>

<p>
    The most important: when included into a <a href="{{ route('docs.forms') }}">Form</a>, it automatically enforces the <x-larastrap::value>enctype="multipart/form-data"</x-larastrap::value> attribute to the parent Form itself.
</p>

@include('partials.example', ['snippet' => 'files.base'])

<x-larastrap::title label="multiple" />

<p>
    The <x-larastrap::parameter>multiple</x-larastrap::parameter> parameter enforces the <x-larastrap::value>multiple</x-larastrap::value> HTML attribute to the node, and - if required - handles the node's <x-larastrap::parameter>name</x-larastrap::parameter> as an array to actually permit multiple values.
</p>

@include('partials.example', ['snippet' => 'files.multiple'])

@endsection
