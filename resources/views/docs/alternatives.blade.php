@extends('layout.docs', [
    'title' => 'Alternatives | Larastrap',
    'claim' => 'Some alternative for Larastrap',
])

@section('docs')

<h1>Alternatives</h1>

<p class="mt-4 lead">
    <strong>Larastrap</strong> has been inspired by many other works, and probably inspired some one else. Here a list of related projects, to be used both as alternatives or integrations.
</p>

<x-larastrap::title label="Symfony Forms" />

<p>
    A cool library to handle forms as code structures.
</p>
<p>
    The laravel-form-bridge package provides more advanded integration with Laravel (even if using Twig instead of Blade...).
</p>
<p>
    <a href="https://symfony.com/doc/current/forms.html" rel="nofollow">https://symfony.com/doc/current/forms.html</a><br>
    <a href="https://github.com/barryvdh/laravel-form-bridge" rel="nofollow">https://github.com/barryvdh/laravel-form-bridge</a>
</p>

<x-larastrap::title label="TallstackUI" />

<p>
    Somehow similar to Larastrap (and announced after Larastrap itself), but based on the Tailwind CSS framework.
</p>
<p>
    Implements a code-oriented approach for personalization of reusable components.
</p>
<p>
    <a href="https://tallstackui.com/" rel="nofollow">https://tallstackui.com/</a>
</p>

<x-larastrap::title label="Laravel UI" />

<p>
    Not an alternative, but probably the most easy way to install Bootstrap on your Laravel project.
</p>
<p>
    Includes all utilities to provide, out of the box, login, registration, password reset and more, all implemented with simple Bootstrap templates.
</p>

<p>
    <a href="https://github.com/laravel/ui" rel="nofollow">https://github.com/laravel/ui</a>
</p>

<x-larastrap::title label="Laravel Breadcrumbs" />

<p>
    Yet not an alternative, but a complementary project: it implements breadcrumbs and supports Bootstrap5 theming.
</p>
<p>
    It does it so well, that it is the actual reason why Larastrap doesn't include a "Breadcrumb" component: just install it!
</p>
<p>
    <a href="https://github.com/diglactic/laravel-breadcrumbs" rel="nofollow">https://github.com/diglactic/laravel-breadcrumbs</a>
</p>

@endsection
