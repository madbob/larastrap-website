@extends('layout.docs', [
    'title' => 'Fields | Larastrap',
    'claim' => 'How to use the Boostrap5 Fields component in Laravel',
])

@section('docs')

<h1>Fields</h1>

<p class="mt-4 lead">
    A <x-larastrap::element>x-larastrap::field</x-larastrap::element> usually wraps an input within a <x-larastrap::element>x-larastrap::form</x-larastrap::element>, and is responsible for the layout of the input itself and his label. Each input field provides to init his own wrapping <x-larastrap::element>x-larastrap::field</x-larastrap::element> (until the <x-larastrap::parameter>squeeze</x-larastrap::parameter> parameter is not set to <x-larastrap::value>true</x-larastrap::value>), and there is no need to manually place it, but it is possible to create your own fields to wrap custom elements.
</p>

<p class="lead">
    All of the following parameters can be passed to an <a href="{{ route('docs.input') }}">Input</a> element, to be applied to his implicit wrapping Field.
</p>

<x-larastrap::title label="label" />

<p>
    The base parameter for a field is his <x-larastrap::parameter>label</x-larastrap::parameter>, used as a description for the child content (which usually is an input box, but can be anything else).
</p>

@include('partials.example', ['snippet' => 'fields.base'])

<x-larastrap::title label="label_class" />

<p>
    The label can be customized assigning one or more CSS classes, using the <x-larastrap::parameter>label_class</x-larastrap::parameter> parameter.
</p>

@include('partials.example', ['snippet' => 'fields.labelclass'])

<x-larastrap::title label="label_html" />

<p>
    If you want to use a completely custom HTML label, you have to use the <x-larastrap::parameter>label_html</x-larastrap::parameter> parameter instead of <x-larastrap::parameter>label</x-larastrap::parameter>. Entities in plain <x-larastrap::parameter>label</x-larastrap::parameter> are escaped; when using <x-larastrap::parameter>label_html</x-larastrap::parameter>, those are not.
</p>

@include('partials.example', ['snippet' => 'fields.labelhtml'])

<x-larastrap::title label="label / input width" />

<p>
    With the <x-larastrap::parameter>label_width</x-larastrap::parameter> and <x-larastrap::parameter>input_width</x-larastrap::parameter> parameters it is possible to define the width of each column, within the twelve columns system of Bootstrap. You can specify both an absolute value, or an array of breakpoints to obtain a responsive layout.
</p>

@include('partials.example', ['snippet' => 'fields.widths'])

<p class="mt-4">
    Those parameter are inherited from the parent <a href="{{ route('docs.containers') }}">Container</a>, to avoid to specify for each field. Even better: it is recommended to define those value in the <a href="{{ route('docs.getting-started', '#configure') }}">Larastrap's global configuration</a>, once and for all the application, and override them only when required for specific and occasional edge cases.
</p>

@include('partials.example', ['snippet' => 'fields.formwidths'])

<p class="mt-4">
    And, as it is quite unusual, you can also specify <x-larastrap::parameter>label_width</x-larastrap::parameter> and <x-larastrap::parameter>input_width</x-larastrap::parameter> for any given <a href="{{ route('docs.input') }}">Input</a> component.
</p>

@include('partials.example', ['snippet' => 'fields.inputwidths'])

<x-larastrap::title label="margins" />

<p>
    <x-larastrap::parameter>margins</x-larastrap::parameter> permits to define margins of the field row within <x-larastrap::value>horizontal</x-larastrap::value> and <x-larastrap::value>vertical</x-larastrap::value> forms. It is expressed as an array (or a space separated string) of four integers, representing top, right, bottom and left margin using <a href="https://getbootstrap.com/docs/5.3/utilities/spacing/">Bootstrap margins sizes</a>.
</p>

@include('partials.example', ['snippet' => 'fields.margins'])

<p class="mt-4">
    Those parameter are inherited from the parent <a href="{{ route('docs.containers') }}">Container</a>.
</p>

@include('partials.example', ['snippet' => 'fields.formmargin'])

<p class="mt-4">
    More usually, you may want to just set all margins to 0 but the bottom one in the <a href="{{ route('docs.getting-started', '#configure') }}">Larastrap's global configuration</a> of "field".
</p>

<x-larastrap::title label="help" />

<p>
    The <x-larastrap::parameter>help</x-larastrap::parameter> parameter implements a classic Bootstrap's pattern: displays a muted text below the actual content, to be used as an explicit hint for the user.
</p>

@include('partials.example', ['snippet' => 'fields.help'])

<x-larastrap::title label="pophelp" />

<p>
    The <x-larastrap::parameter>pophelp</x-larastrap::parameter> parameter is an alternative to <x-larastrap::parameter>help</x-larastrap::parameter>, and provides a popover to be used for more detailed (or less invasive) descriptions of the purpose of each field.
</p>
<p>
    Remember you have to init the Javascript function to handle Bootstrap's popovers!
</p>

@include('partials.example', ['snippet' => 'fields.pophelp'])

<p class="mt-4">
    It is possible to overwrite once the aspect of the <x-larastrap::parameter>pophelp</x-larastrap::parameter> element directly from the global configuration file, to change the <x-larastrap::parameter>symbol</x-larastrap::parameter> and the CSS classes.
</p>

<?php

app()->make('LarastrapStack')->overrideNodesConfig([
    'pophelp' => [
        'symbol' => 'HELP!',
        'override_classes' => ['badge', 'bg-success', 'float-start', 'me-2'],
    ]
]);

?>

@include('partials.example', ['snippet' => 'fields.custompophelp'])

<p class="mt-4">
    Also <x-larastrap::parameter>symbol_html</x-larastrap::parameter> can be used, to specify a HTML string to be used as label (for example: a Bootstrap icon).
</p>

<?php

app()->make('LarastrapStack')->overrideNodesConfig(null);

?>

<?php

app()->make('LarastrapStack')->overrideNodesConfig([
    'pophelp' => [
        'symbol_html' => '<i class="bi bi-info-circle-fill"></i>',
        'override_classes' => ['text-danger'],
    ]
]);

?>

@include('partials.example', ['snippet' => 'fields.custompophelp2'])

<?php

app()->make('LarastrapStack')->overrideNodesConfig(null);

?>

<x-larastrap::title label="do_label" />

<p>
    The <x-larastrap::parameter>do_label</x-larastrap::parameter> parameter can be valorized to
</p>
<ul>
    <li><x-larastrap::value>show</x-larastrap::value> - the default: the label is formatted accondingly the the parent's <x-larastrap::parameter>formview</x-larastrap::parameter>)</li>
    <li><x-larastrap::value>hide</x-larastrap::value> - the label is omitted from the final output, but the field's content keeps his size</li>
    <li><x-larastrap::value>empty</x-larastrap::value> - the label is appended to the HTML, but contains an empty string despite any other configuration</li>
    <li><x-larastrap::value>squeeze</x-larastrap::value> - both label and wrapping field are totally omitted</li>
</ul>

@include('partials.example', ['snippet' => 'fields.dolabel'])

<p class="mt-4">
    Here a more exaustive example, using a <x-larastrap::element>x-larastrap::text</x-larastrap::element> as a reference.
</p>

@include('partials.example', ['snippet' => 'fields.dolabelinput'])

<x-larastrap::title label="squeeze" />

<p>
    <x-larastrap::parameter>squeeze</x-larastrap::parameter> parameter is just a convenient shorthand for <x-larastrap::parameter>do_label="squeeze"</x-larastrap::parameter>.
</p>

@include('partials.example', ['snippet' => 'fields.squeeze'])

<p class="mt-4">
    This is mostly intended for usage within an actual input, and setup it to omit the field when a label is not required (e.g. within a table).
</p>

@include('partials.example', ['snippet' => 'fields.table'])

<x-larastrap::title label="inner prepend / append" />

<p>
    Field is a Container, and it supports <x-larastrap::parameter>prependNodes</x-larastrap::parameter>/<x-larastrap::parameter>appendNodes</x-larastrap::parameter> parameters to attach items before and after the element itself. But your can also use <x-larastrap::parameter>innerPrependNodes</x-larastrap::parameter>/<x-larastrap::parameter>innerAppendNodes</x-larastrap::parameter> parameters to append nodes inside the field itself, so to be formatted as any other common component.
</p>
<p>
    The most interesting use case is the ability to define once your own composite widgets (in form of <a href="{{ route('docs.custom-elements') }}">Custom Element</a>) and let it flow seamlessy within your forms.
</p>

@include('partials.custom', ['snippet' => 'customs.compositefield'])

@endsection
