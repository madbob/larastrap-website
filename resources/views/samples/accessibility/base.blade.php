<x-larastrap::text label="Without explicit aria-label" squeeze />
<br>
<x-larastrap::text label="With explicit aria-label" :attributes="['aria-label' => 'This is the label']" squeeze />
