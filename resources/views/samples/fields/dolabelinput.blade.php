<x-larastrap::text placeholder="Normal field, input + label" label="Test 1" />
<x-larastrap::text placeholder="Label is hidden, input has the same width" label="Test 2" do_label="hide" />
<x-larastrap::text placeholder="Label is blank, but has allocated width" label="Test 3" do_label="empty" />
<x-larastrap::text placeholder="No label at all, full width input" label="Test 4" do_label="squeeze" />
