<x-larastrap::field label="Name" label_width="6" input_width="6">
    <img src="https://picsum.photos/100">
</x-larastrap::field>

<br>

<x-larastrap::field label="Name" label_width="2" input_width="10">
    <img src="https://picsum.photos/100">
</x-larastrap::field>

<br>

<x-larastrap::field label="Name" :label_width="['md' => 4, 'lg' => 3]" :input_width="['md' => 8, 'lg' => 9]">
    <img src="https://picsum.photos/100">

    <p>
        Here, the width of label's and input's columns vary accordingly to the responsive breakpoint.
    </p>
</x-larastrap::field>
