<!--

In your config/larastrap.php file...

return [
    'pophelp' => [
        'symbol_html' => '<i class="bi bi-info-circle-fill"></i>',
        'override_classes' => ['text-danger'],
    ]
];

-->

<x-larastrap::field label="Name" pophelp="This is a hoverable help box">
    <img src="https://picsum.photos/100">
</x-larastrap::field>
