<x-larastrap::field label="Normal Label" do_label="show">
    <img src="https://picsum.photos/100">
    <p>Normal behavior</p>
</x-larastrap::field>

<x-larastrap::field label="Name" do_label="hide">
    <img src="https://picsum.photos/100">
    <p>Hidden label: in a horizontal form, the content is shifted to the left due the missing node</p>
</x-larastrap::field>

<x-larastrap::field label="Name" do_label="empty">
    <img src="https://picsum.photos/100">
    <p>Empty label: in a horizontal form, the space allocation for the label is still preserved</p>
</x-larastrap::field>

<x-larastrap::field label="Name" do_label="squeeze">
    <img src="https://picsum.photos/100">
    <p>No label nor wrapping field (.row and .col divs)</p>
</x-larastrap::field>
