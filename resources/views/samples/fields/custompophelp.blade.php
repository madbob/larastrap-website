<!--

In your config/larastrap.php file...

return [
    'pophelp' => [
        'symbol' => 'HELP!',
        'override_classes' => ['badge', 'bg-success', 'float-start', 'me-2'],
    ]
];

-->

<x-larastrap::field label="Name" pophelp="This is a hoverable help box">
    <img src="https://picsum.photos/100">
</x-larastrap::field>
