<table class="table">
    <tbody>
        @foreach(App\Models\User::take(3)->get() as $user)
            <x-larastrap::enclose :obj="$user">
                <tr>
                    <td>
                        <x-larastrap::text name="name" squeeze />
                    </td>
                    <td>
                        <x-larastrap::text name="email" squeeze />
                    </td>
                </tr>
            </x-larastrap::enclose>
        @endforeach
    </tbody>
</table>
