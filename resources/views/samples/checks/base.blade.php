@php

$options = ['red' => 'Red', 'green' => 'Green', 'blue' => 'Blue'];

@endphp

<x-larastrap::checks label="Choose Many" :options="$options" />
<x-larastrap::radios label="Choose One" :options="$options" />
