@php

$options = [
    'first' => (object) [
        'label' => 'First',
        // This is intentionally broken, as the radio button will change the
        // checkbox having the same ID (as the $options array is here used to
        // init both components)
        // Be aware of IDs you assign!
        'id' => 'list_first',
    ],
    'second' => (object) [
        'label' => 'Second',
        'disabled' => true,
    ],
    'third' => (object) [
        'label' => 'Third',
        'attributes' => ['data-bs-toggle' => 'tooltip', 'data-bs-title' => 'Sample tooltip'],
    ],
    'fourth' => (object) [
        'label' => 'Fourth',
        'hidden' => true,
    ],
    'fifth' => (object) [
        'label' => 'Fifth',
        'classes' => ['text-white', 'bg-danger'],
    ],
    'sixth' => (object) [
        'label_html' => '<s>Sixth</s>',
    ],
];

@endphp

<x-larastrap::radiolist label="Choose One" :options="$options" />
<x-larastrap::checklist label="Choose Many" :options="$options" />
