@php

// In this example, the User model has a "boss" relationship defined as
//
// public function boss()
// {
//     return $this->belongsTo(User::class);
// }
//
// and a "friends" relationship defined as
//
// public function friends()
// {
//     return $this->belongsToMany(User::class);
// }

$ref = App\Models\User::has('boss')->inRandomOrder()->first();
$bosses = App\Models\User::doesntHave('boss')->orderBy('name', 'asc')->get();
$users = App\Models\User::orderBy('name', 'asc')->get();

@endphp

<x-larastrap::form :obj="$ref">
    <x-larastrap::radios-model name="boss" label="Select a Boss" :options="$bosses" />
    <x-larastrap::checklist-model name="friends" label="Select friends" :options="$users" />
</x-larastrap::form>
