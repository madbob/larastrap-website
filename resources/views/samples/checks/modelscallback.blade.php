@php

$users = App\Models\User::inRandomOrder()->take(3)->get();
$translate = fn($obj) => [$obj->id, $obj->email];

@endphp

<x-larastrap::radiolist-model label="Select a User" :options="$users" :translateCallback="$translate" />
