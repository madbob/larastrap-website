<x-larastrap::form formview="horizontal">
    <div class="alert alert-info">Horizontal form, non-inline checkbox</div>
    <x-larastrap::text label="Reference Text" />
    <x-larastrap::check label="Non-Inline Check" />
</x-larastrap::form>

<hr>

<x-larastrap::form formview="vertical">
    <div class="alert alert-info">Vertical form, non-inline checkbox</div>
    <x-larastrap::text label="Reference Text" />
    <x-larastrap::check label="Non-Inline Check" />
</x-larastrap::form>

<hr>

<x-larastrap::form formview="horizontal">
    <div class="alert alert-info">Horizontal form, inline checkbox</div>
    <x-larastrap::text label="Reference Text" />
    <x-larastrap::check label="Inline Check" inline />
</x-larastrap::form>

<hr>

<x-larastrap::form formview="horizontal">
    <div class="alert alert-info">Horizontal form, inline checkbox aligned to the beginning</div>
    <x-larastrap::text label="Reference Text" />
    <x-larastrap::check label="Inline Check" inline do_label="hide" />
</x-larastrap::form>

<hr>

<x-larastrap::form formview="vertical">
    <div class="alert alert-info">Vertical form, inline checkbox</div>
    <x-larastrap::text label="Reference Text" />
    <x-larastrap::check label="Inline Check" inline />
</x-larastrap::form>
