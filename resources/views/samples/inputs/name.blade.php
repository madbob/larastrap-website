@php

$obj = App\Models\User::inRandomOrder()->first();

@endphp

<x-larastrap::form :obj="$obj">
    <x-larastrap::text name="name" label="Name" />
</x-larastrap::form>
