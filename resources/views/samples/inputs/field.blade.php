<x-larastrap::text label="A Text" help="This is a text input" />
<x-larastrap::text label="Another Text" pophelp="This is another text input" />
<x-larastrap::text label_html="<strong>HTML label</strong>" label_class="text-danger" />
<x-larastrap::text placeholder="Full width input" squeeze />
