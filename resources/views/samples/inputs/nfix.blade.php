@php

$users = App\Models\User::inRandomOrder()->take(3)->get();

@endphp

<x-larastrap::form>
    @foreach($users as $user)
        <x-larastrap::enclose :obj="$user">
            <x-larastrap::text name="name" label="Name" npostfix="[]" />
        </x-larastrap::enclose>
    @endforeach
</x-larastrap::form>
