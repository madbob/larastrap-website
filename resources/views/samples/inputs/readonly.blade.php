<x-larastrap::text name="name" label="Name (readonly)" readonly value="Test" />
<x-larastrap::text name="name" label="Name (disabled)" disabled value="Test" />
<x-larastrap::text name="name" label="Name (readonly not text)" readonly asplaintext="false" value="Test" />
<x-larastrap::text name="name" label="Name (disabled not text)" disabled asplaintext="false" value="Test" />
