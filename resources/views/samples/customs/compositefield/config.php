[
    'customs' => [
        'date-and-time' => [
            'extends' => 'field',
            'params' => [
                'innerAppendNodes' => [
                    [
                        'extends' => 't',
                        'params' => [
                            'node' => 'div',
                            'classes' => ['row'],
                            'innerAppendNodes' => [
                                [
                                    'extends' => 't',
                                    'params' => [
                                        'node' => 'div',
                                        'classes' => ['col'],
                                        'innerAppendNodes' => [
                                            [
                                                'extends' => 'time',
                                                'params' => [
                                                    'name' => 'time',
                                                    'squeeze' => true,
                                                ]
                                            ],
                                        ],
                                    ],
                                ],
                                [
                                    'extends' => 't',
                                    'params' => [
                                        'node' => 'div',
                                        'classes' => ['col'],
                                        'innerAppendNodes' => [
                                            [
                                                'extends' => 'select',
                                                'params' => [
                                                    'name' => 'timezone',
                                                    'squeeze' => true,
                                                    'options' => function() {
                                                        $opt = [];

                                                        foreach(\DateTimeZone::listIdentifiers() as  $zoneLabel) {
                                                            $opt[$zoneLabel] = $zoneLabel;
                                                        }

                                                        return $opt;
                                                    }
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ]
]
