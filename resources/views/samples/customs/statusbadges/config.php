[
    'customs' => [
        'badge_ok' => [
            'extends' => 't',
            'params' => [
                'node' => 'span',
                'classes' => ['badge', 'text-bg-success'],
                'content' => 'OK',
            ],
        ],
        'badge_ko' => [
            'extends' => 't',
            'params' => [
                'node' => 'span',
                'classes' => ['badge', 'text-bg-danger'],
                'content' => 'NO GOOD',
            ],
        ],
    ],
]
