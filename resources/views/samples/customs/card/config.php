[
    'customs' => [
        'ctitle' => [
            'extends' => 't',
            'params' => [
                'node' => 'h5',
                'classes' => ['card-title'],
                'name' => 'name',
            ]
        ],
        'cmail' => [
            'extends' => 't',
            'params' => [
                'node' => 'p',
                'classes' => ['card-text'],
                'name' => 'email',
                'prelabel' => '<i class="bi bi-envelope"></i> '
            ]
        ],
    ]
]
