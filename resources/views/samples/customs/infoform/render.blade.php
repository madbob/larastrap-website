@php
$user = App\Models\User::inRandomOrder()->first();
@endphp

<x-larastrap::informativeform :obj="$user">
    <x-larastrap::text name="name" label="Name" />
    <x-larastrap::email name="email" label="E-Mail" />
</x-larastrap::informativeform>
