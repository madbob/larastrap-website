[
    'customs' => [
        'informativeform' => [
            'extends' => 'form',
            'params' => [
                'prependNodes' => [
                    [
                        'extends' => 'larastrap::t',
                        'params' => [
                            'node' => 'div',
                            'classes' => ['alert', 'alert-info'],
                            'innerAppendNodes' => [
                                [
                                    'extends' => 'larastrap::t',
                                    'params' => [
                                        'node' => 'p',
                                        'classes' => ['m-0'],
                                        'prelabel' => 'Internal ID: ',
                                        'name' => 'id',
                                    ],
                                ]
                            ]
                        ],
                    ],
                ],
                'appendNodes' => [
                    [
                        'extends' => 'larastrap::text',
                        'params' => [
                            'label' => 'Created At',
                            'name' => 'created_at',
                            'readonly' => true,
                        ]
                    ],
                    [
                        'extends' => 'larastrap::text',
                        'params' => [
                            'label' => 'Last Update',
                            'name' => 'updated_at',
                            'readonly' => true,
                        ],
                    ],
                ],
            ],
        ],
    ],
]
