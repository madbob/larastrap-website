@php
$users = App\Models\User::inRandomOrder()->take(5)->select(['name', 'id'])->get();
@endphp

<x-larastrap::user-select />

<script>

let users = {!! json_encode($users) !!}
let button = document.querySelector('.user-name');
button.addEventListener('click', () => {
    let user = users[Math.floor(Math.random() * users.length)];
    button.innerText = user.name;
    document.querySelector('input[name=user_id]').value = user.id;
});

</script>
