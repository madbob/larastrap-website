[
    'customs' => [
        'user-select' => [
            'extends' => 'enclose',
            'params' => [
                'appendNodes' => [
                    [
                        'extends' => 'button',
                        'params' => [
                            'classes' => ['user-name'],
                            'label' => 'Click to select a user',
                        ],
                    ],
                    [
                        'extends' => 'hidden',
                        'params' => [
                            'name' => 'user_id',
                            'value' => '',
                        ],
                    ],
                ],
                'reviewCallback' => function($element, $params) {
                    $obj = $params['obj'];

                    if ($obj) {
                        foreach($params['appendNodes'] as &$appended) {
                            if ($appended['extends'] == 'button') {
                                $appended['params']['label'] = $obj->name;
                            }
                            else if ($appended['extends'] == 'hidden') {
                                $appended['params']['value'] = $obj->id;
                            }
                        }
                    }

                    return $params;
                }
            ],
        ],
    ],
]
