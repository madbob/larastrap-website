@php
$users = App\Models\User::inRandomOrder()->take(2)->get();
@endphp

<div class="card-group">
    @foreach($users as $user)
        <x-larastrap::easy-user-card :obj="$user" />
    @endforeach
</div>
