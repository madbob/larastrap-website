[
    'customs' => [
        'easy-user-card' => [
            'extends' => 't',
            'params' => [
                'node' => 'div',
                'classes' => ['card'],
                'innerAppendNodes' => [
                    [
                        'extends' => 't',
                        'params' => [
                            'node' => 'div',
                            'classes' => ['card-body'],
                            'innerAppendNodes' => [
                                [
                                    'extends' => 't',
                                    'params' => [
                                        'node' => 'h5',
                                        'classes' => ['card-title'],
                                        'name' => 'name',
                                    ],
                                ],
                                [
                                    'extends' => 't',
                                    'params' => [
                                        'node' => 'p',
                                        'classes' => ['card-text'],
                                        'name' => 'email',
                                        'prelabel' => '<i class="bi bi-envelope"></i> '
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
]
