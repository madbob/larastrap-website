@php

/*

namespace App\Models;

use MadBob\Larastrap\Integrations\AutoReadsFields;
use MadBob\Larastrap\Integrations\AutoReadOperation;

class User implements AutoReadsFields
{
    public function autoreadField(string $name, Request $request): AutoReadOperation
    {
        if ($name == 'avatar') {
            Session::flash('avatar_file', $request->file('avatar')->getClientOriginalName());
            return AutoReadOperation::Managed;
        }

        return AutoReadOperation::Auto;
    }
}

*/

@endphp

@if(Session::has('avatar_file'))
    <div class="alert alert-info">
        Uploaded the file {{ Session::get('avatar_file') }}<br>
        Nothing has been saved on the server!
    </div>
@else
    <x-larastrap::form route="autoread_save" error_bag="file" novalidate autoread>
        <x-larastrap::hidden name="type" value="file" skip_autoread />
        <x-larastrap::text name="name" label="Name" required />
        <x-larastrap::email name="email" label="E-Mail" required />
        <x-larastrap::password name="password" label="Password" required />
        <x-larastrap::file name="avatar" label="Avatar" required />
    </x-larastrap::form>
@endif
