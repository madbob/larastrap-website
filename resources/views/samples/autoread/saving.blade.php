@php

/*

The "autoread_save" route is implemented by the following method:

public function autoreadSaving(Request $request)
{
    app()->make('LarastrapStack')->autoreadValidate($request, true);

    DB::beginTransaction();
    $user = app()->make('LarastrapStack')->autoreadSave($request, User::class);
    $user->save();
    $user->load('boss', 'friends');
    DB::rollback();

    $type = $request->input('type');
    $feedback = match($type) {
        'save' => 'saved_attributes',
        'create' => 'created_attributes',
        'file' => 'file_attributes',
    };

    return back()->with($feedback, [
        'name' => $user->name,
        'email' => $user->email,
        'password' => $user->password,
        'boss' => $user->boss?->name,
        'friends' => $user->friends->map(fn($f) => $f->name)->join(','),
    ])->withInput();
}

It is not actually saved on the database as this is just for demo purpose...

*/

$user = App\Models\User::inRandomOrder()->has('boss')->first();
$bosses = App\Models\User::doesntHave('boss')->orderBy('name', 'asc')->get();
$users = App\Models\User::orderBy('name', 'asc')->get();

@endphp

@if(Session::has('saved_attributes'))
    <div class="alert alert-info">
        <p>
            Saved attributes:
        </p>

        <ul>
            @foreach(Session::get('saved_attributes') as $name => $value)
                <li>{{ $name }} = {{ $value }}</li>
            @endforeach
        </ul>
    </div>
@else
    <x-larastrap::form route="autoread_save" error_bag="save" :obj="$user" novalidate autoread>
        <x-larastrap::hidden name="type" value="save" skip_autoread />
        <x-larastrap::text nprefix="save_test_" name="name" label="Name" required />
        <x-larastrap::email nprefix="save_test_" name="email" label="E-Mail" required />
        <x-larastrap::radios-model nprefix="save_test_" name="boss" label="Select a Boss" :options="$bosses" required />
        <x-larastrap::checklist-model nprefix="save_test_" name="friends" label="Select friends" :options="$users" />
    </x-larastrap::form>
@endif
