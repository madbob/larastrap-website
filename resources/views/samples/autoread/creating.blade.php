@php

/*

Note that here we use the same "autoread_save" route already involved in the
previous example, even if the form has not explicit Model assigned and input
names as different.

*/

$bosses = App\Models\User::doesntHave('boss')->orderBy('name', 'asc')->get();
$users = App\Models\User::orderBy('name', 'asc')->get();

@endphp

@if(Session::has('created_attributes'))
    <div class="alert alert-info">
        <p>
            Saved attributes:
        </p>

        <ul>
            @foreach(Session::get('created_attributes') as $name => $value)
                <li>{{ $name }} = {{ $value }}</li>
            @endforeach
        </ul>
    </div>
@else
    <x-larastrap::form route="autoread_save" error_bag="create" novalidate autoread>
        <x-larastrap::hidden name="type" value="create" skip_autoread />
        <x-larastrap::text nprefix="create_test_" name="name" label="Name" required />
        <x-larastrap::email nprefix="create_test_" name="email" label="E-Mail" required />
        <x-larastrap::password nprefix="create_test_" name="password" label="Password" required />
        <x-larastrap::radios-model nprefix="create_test_" name="boss" label="Select a Boss" :options="$bosses" required />
        <x-larastrap::checklist-model nprefix="create_test_" name="friends" label="Select friends" :options="$users" />
    </x-larastrap::form>
@endif
