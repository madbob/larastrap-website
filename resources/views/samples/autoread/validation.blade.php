@php

/*

The "autoread_validate" route is implemented by the following method:

public function autoreadValidation(Request $request)
{
    app()->make('LarastrapStack')->autoreadValidate($request, true);
    return back()->withInput();
}

*/

@endphp

<x-larastrap::form route="autoread_validate" error_bag="validate" novalidate autoread>
    <x-larastrap::text name="text" label="A Text" required />
    <x-larastrap::textarea name="textarea" label="A Textarea" required />
    <x-larastrap::number name="number" label="A Number" required min="5" max="50" help="Must be between 5 and 50" />
    <x-larastrap::email name="email" label="An Email" required />
    <x-larastrap::password name="password" label="A Password" required minlength="8" help="At least 8 characters"  />
    <x-larastrap::url name="url" label="An URL" required />
    <x-larastrap::tel name="phone" label="A Phone Number" required />
    <x-larastrap::date name="date" label="A Date" required />
    <x-larastrap::time name="time" label="A Time" required />
    <x-larastrap::datetime name="datetime" label="Both Date and Time" required />
    <x-larastrap::month name="month" label="A Month" required />
    <x-larastrap::week name="week" label="A Week" required />
    <x-larastrap::check name="boolean" label="A Boolean" required />
    <x-larastrap::checks name="choose" label="At least one" :options="['red' => 'Red', 'green' => 'Green', 'blue' => 'Blue']" required />
</x-larastrap::form>
