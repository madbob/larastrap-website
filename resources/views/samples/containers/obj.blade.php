@php

$obj = (object) ['name' => 'Foo', 'bio' => 'A very smart guy!', 'email' => 'foo@bar.baz'];

@endphp

<x-larastrap::accordion :obj="$obj">
    <x-larastrap::accordionitem label="Contacts">
        <x-larastrap::text name="name" readonly squeeze />
        <x-larastrap::text name="email" readonly squeeze />
    </x-larastrap::accordionitem>
    <x-larastrap::accordionitem label="Bio">
        <x-larastrap::text name="bio" readonly squeeze />
    </x-larastrap::accordionitem>
</x-larastrap::accordion>
