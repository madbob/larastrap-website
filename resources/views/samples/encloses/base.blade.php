@php
$obj = App\Models\User::inRandomOrder()->first();
@endphp

<x-larastrap::enclose :obj="$obj">
    <x-larastrap::text name="name" readonly squeeze />
    <x-larastrap::email name="email" squeeze />
</x-larastrap::enclose>
