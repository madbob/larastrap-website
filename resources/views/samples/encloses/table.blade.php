<x-larastrap::form>
    <table class="table">
        @foreach(App\Models\User::inRandomOrder()->take(3)->get() as $user)
            <x-larastrap::enclose :obj="$user">
                <tr>
                    <td><x-larastrap::text name="name" readonly squeeze="true" /></td>
                    <td><x-larastrap::email name="email" npostfix="[]" squeeze="true" /></td>
                </tr>
            </x-larastrap::enclose>
        @endforeach
    </table>
</x-larastrap::form>
