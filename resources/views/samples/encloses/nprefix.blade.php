<x-larastrap::form>
    @foreach(App\Models\User::inRandomOrder()->take(3)->get() as $user)
        <x-larastrap::enclose :obj="$user" nprefix="user_" npostfix="[]">
            <div class="row mb-2">
                <div class="col">
                    <x-larastrap::hidden name="id" squeeze />
                    <x-larastrap::email name="email" squeeze />
                </div>
            </div>
        </x-larastrap::enclose>
    @endforeach
</x-larastrap::form>
