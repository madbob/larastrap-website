@php

$users = App\Models\User::inRandomOrder()->take(3)->get();

@endphp

<x-larastrap::select-model label="Select a User" :options="$users" />
