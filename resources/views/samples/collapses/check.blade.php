<x-larastrap::check triggers_collapse="test_checkbox_collapse_closed" label="This is unchecked" />
<x-larastrap::collapse id="test_checkbox_collapse_closed">
    <p class="p-2">This is a collapse!</p>
</x-larastrap::collapse>

<x-larastrap::check triggers_collapse="test_checkbox_collapse_opened" label="This is checked" checked />
<x-larastrap::collapse id="test_checkbox_collapse_opened">
    <p class="p-2">This is a collapse!</p>
</x-larastrap::collapse>
