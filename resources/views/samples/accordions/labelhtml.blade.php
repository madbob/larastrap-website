<x-larastrap::accordion>
    <x-larastrap::accordionitem label_html="<i class='bi bi-1-square-fill'></i>">
        <p>This is the first accordion</p>
    </x-larastrap::accordionitem>

    <x-larastrap::accordionitem label_html="<i class='bi bi-2-circle-fill'></i>">
        <p>This is the second accordion</p>
    </x-larastrap::accordionitem>

    <x-larastrap::accordionitem label_html="<i class='bi bi-3-square'></i>">
        <p>This is the third accordion</p>
    </x-larastrap::accordionitem>
</x-larastrap::accordion>
