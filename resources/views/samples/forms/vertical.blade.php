<x-larastrap::form formview="vertical">
    <x-larastrap::text name="firstname" label="First Name" />
    <x-larastrap::text name="lastname" label="Last Name" />
    <x-larastrap::email name="email" label="EMail" />
    <x-larastrap::check name="check" label="Check" inline />
    <x-larastrap::checks name="checks" label="Checks" :options="['One', 'Two', 'Three']" />
</x-larastrap::form>
