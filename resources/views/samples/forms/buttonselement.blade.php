@php

$buttons = [
    ['element' => 'larastrap::link', 'label' => 'This is a Link', 'href' => 'https://larastrap.madbob.org/'],
    ['color' => 'success', 'label' => 'Submit', 'attributes' => ['type' => 'submit']]
];

@endphp

<x-larastrap::form :buttons="$buttons">
    <x-larastrap::text name="firstname" label="First Name" />
    <x-larastrap::text name="lastname" label="Last Name" />
    <x-larastrap::email name="email" label="EMail" />
</x-larastrap::form>
