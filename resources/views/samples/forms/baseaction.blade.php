<x-larastrap::form :obj="null" baseaction="user">
    <div class="alert alert-info">
        This form creates a new model.
    </div>

    <x-larastrap::text name="name" label="Name" />
    <x-larastrap::email name="email" label="EMail" />
</x-larastrap::form>

<br>

@php

$obj = (object) [
    'id' => 123,
    'name' => 'Foo Bar',
    'email' => 'foobar@example.com',
];

@endphp

<x-larastrap::form :obj="$obj" baseaction="user">
    <div class="alert alert-info">
        This form updates the existing model.
    </div>

    <x-larastrap::text name="name" label="Name" />
    <x-larastrap::email name="email" label="EMail" />
</x-larastrap::form>
