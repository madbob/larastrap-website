<x-larastrap::form formview="squeeze">
    <table class="table">
        <tbody>
            @foreach(App\Models\User::take(3)->get() as $user)
                <x-larastrap::enclose :obj="$user">
                    <tr>
                        <td>
                            <x-larastrap::text name="name" />
                        </td>
                        <td>
                            <x-larastrap::text name="email" />
                        </td>
                    </tr>
                </x-larastrap::enclose>
            @endforeach
        </tbody>
    </table>
</x-larastrap::form>
