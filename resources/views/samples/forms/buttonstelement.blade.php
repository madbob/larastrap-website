@php

$user = App\Models\User::inRandomOrder()->first();

$buttons = [
    ['element' => 'larastrap::t', 'node' => 'small', 'name' => 'updated_at', 'prelabel' => 'Last update: ', 'classes' => ['float-start']],
    ['color' => 'success', 'label' => 'Submit', 'attributes' => ['type' => 'submit']]
];

@endphp

<x-larastrap::form :buttons="$buttons" :obj="$user">
    <x-larastrap::text name="name" label="Name" />
    <x-larastrap::email name="email" label="EMail" />
    <hr>
</x-larastrap::form>
