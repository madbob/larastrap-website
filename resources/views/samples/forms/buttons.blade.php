<x-larastrap::form :buttons="[['color' => 'success', 'label' => 'Submit', 'attributes' => ['type' => 'submit']], ['color' => 'danger', 'label' => 'Reset', 'attributes' => ['type' => 'reset']]]">
    <x-larastrap::text name="firstname" label="First Name" />
    <x-larastrap::text name="lastname" label="Last Name" />
    <x-larastrap::email name="email" label="EMail" />
</x-larastrap::form>
