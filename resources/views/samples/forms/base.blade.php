@php

$obj = (object) ['id' => 1234, 'name' => 'Foo', 'surname' => 'Baz', 'bio' => 'A very smart guy!', 'email' => 'foo@bar.baz', 'country' => 'it', 'colors' => ['red', 'blue']];
$countries = ['fr' => 'France', 'de' => 'Germany', 'it' => 'Italy'];

@endphp

<x-larastrap::form :obj="$obj">
    <x-larastrap::hidden name="id" />
    <x-larastrap::number name="id" label="ID" disabled="true" />
    <x-larastrap::text name="name" label="Name" />
    <x-larastrap::text name="surname" label="Surname" value="Overwritten value (should be 'Baz')" />
    <x-larastrap::textarea name="bio" label="Bio" />
    <x-larastrap::email name="email" label="EMail" help="Must be a valid email address" />
    <x-larastrap::check name="check" label="Check" />

    <x-larastrap::select name="country" label="Country (as select)" :options="$countries" />
    <x-larastrap::radios name="country" label="Country (as radio)" :options="$countries" color="danger" />

    <x-larastrap::checks name="colors" label="Colors" :options="['red' => 'Red', 'green' => 'Green', 'blue' => 'Blue']" color="warning" />
</x-larastrap::form>
