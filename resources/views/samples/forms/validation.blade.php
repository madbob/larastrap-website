@php

// Server side validation for this form is defined as:
//
// $request->validateWithBag('special', [
//     'name' => 'required',
//     'email' => 'required|email|max:255',
//     'description' => 'required',
//     'select' => 'required',
//     'check' => 'accepted',
//     'checks' => 'required',
//     'radios' => 'required',
//     'checklist' => 'required',
// ]);

@endphp

<x-larastrap::form action="/validated" error_bag="special">
    <x-larastrap::text name="name" label="Name" />
    <x-larastrap::email name="email" label="EMail" />
    <x-larastrap::textarea name="description" label="Description" />
    <x-larastrap::select name="select" label="Select" :options="['' => 'Choose One', 'First', 'Second']" />
    <x-larastrap::check name="check" label="Check" />
    <x-larastrap::checks name="checks" label="Checks" :options="['One', 'Two', 'Three']" />
    <x-larastrap::radios name="radios" label="Radio" :options="['One', 'Two', 'Three']" />
    <x-larastrap::checklist name="checklist" label="Check List" :options="['One', 'Two', 'Three']" />
</x-larastrap::form>
