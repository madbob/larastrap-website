@php

// Server side validation for this form is defined as:
//
// $request->validate([
//     'person.name' => 'required',
//     'person.email' => 'required|email|max:255',
// ]);

@endphp

<x-larastrap::form action="/validated_dot">
    <x-larastrap::text name="person.name" label="Name" />
    <x-larastrap::email name="person.email" label="EMail" />
</x-larastrap::form>
