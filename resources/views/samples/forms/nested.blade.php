@php

/*

class User
{
    protected $casts = [
        'attributes' => 'array',
    ];
}

*/

$user = App\Models\User::inRandomOrder()->first();

$arbitrary = (object) [
    'name' => 'John Doe',
    'attributes' => [
        'external_id' => '1234567890',
    ]
];

@endphp

<x-larastrap::form :obj="$user">
    <div class="alert alert-info">
        Example with Eloquent model
    </div>

    <x-larastrap::text name="name" label="Name" />
    <x-larastrap::text name="attributes.external_id" label="External ID" />
</x-larastrap::form>

<hr>

<x-larastrap::form :obj="$arbitrary">
    <div class="alert alert-info">
        Example with plain object
    </div>

    <x-larastrap::text name="name" label="Name" />
    <x-larastrap::text name="attributes.external_id" label="External ID" />
</x-larastrap::form>
