<x-larastrap::form method="DELETE" :buttons="[['label' => 'Delete!', 'color' => 'danger', 'type' => 'submit']]">
    <div class="alert alert-danger">This is a delete form!</div>
</x-larastrap::form>
