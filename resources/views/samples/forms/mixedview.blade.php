<x-larastrap::form formview="horizontal">
    <x-larastrap::text name="firstname" label="First Name" />
    <x-larastrap::text name="lastname" label="Last Name" />

    <div class="row">
        <div class="col">
            <x-larastrap::email name="email" label="EMail" formview="vertical" />
            <x-larastrap::tel name="phone" label="Phone" formview="vertical" />
        </div>
        <div class="col">
            <x-larastrap::text name="address" label="Address" formview="vertical" />
            <x-larastrap::text name="city" label="City" formview="vertical" />
        </div>
    </div>
</x-larastrap::form>
