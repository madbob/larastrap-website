<x-larastrap::form id="native-validated" client_side_errors="true">
    <x-larastrap::text name="name" label="Name" required />
    <x-larastrap::email name="email" label="EMail" required />
</x-larastrap::form>

<script>
(() => {
    let form = document.getElementById('native-validated');
    form.addEventListener('submit', event => {
        if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
        }

        form.classList.add('was-validated')
    }, false);
})();
</script>
