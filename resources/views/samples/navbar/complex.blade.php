<x-larastrap::navbar :options="[
    'Home' => ['url' => '/'],
    'Getting Started' => ['route' => 'docs.getting-started', 'active' => true],
    'Base Element' => ['url' => '/docs/components/element', 'attributes' => ['data-bs-toggle' => 'tooltip', 'data-bs-title' => 'Sample tooltip']],
]" />
