@php
$obj = App\Models\User::inRandomOrder()->first();
@endphp

<x-larastrap::form :obj="$obj" disabled>
    <x-larastrap::text name="name" label="Name" />
    <x-larastrap::text name="surname" label="Surname" value="Bar" />
    <x-larastrap::email name="email" label="EMail" help="Must be a valid email address" />
</x-larastrap::form>
