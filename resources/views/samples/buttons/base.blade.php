<x-larastrap::button label="Primary" color="primary" />
<x-larastrap::button label="Secondary" color="secondary" />
<x-larastrap::button label="Success" color="success" />
<x-larastrap::button label="Danger" color="danger" />
<x-larastrap::button label="Warning" color="warning" />
<x-larastrap::button label="Info" color="info" />
<x-larastrap::button label="Light" color="light" />
<x-larastrap::button label="Dark" color="dark" />
<x-larastrap::button label="Link" color="link" />
