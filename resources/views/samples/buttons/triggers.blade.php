<x-larastrap::button label="Open Modal" triggers_modal="#test-modal" />
<x-larastrap::modal title="Test" id="test-modal">
    <p>This is a modal!</p>
</x-larastrap::modal>

<x-larastrap::link triggers_collapse="test-collapse" label="Toggle Collapse" />
<x-larastrap::collapse id="test-collapse">
    <p>This is a collapse!</p>
</x-larastrap::collapse>
