[
    'customs' => [
        'rlink' => [
            'extends' => 'link',
            'params' => [
                'postlabel' => ' <i class="bi bi-box-arrow-up-right"></i>',
                'attributes' => ['target' => '_blank'],
            ]
        ]
    ]
]
