<x-larastrap::tabs active="0">
    <x-larastrap::tabpane label="First" button_classes="bg-danger text-white">
        <p>This is the first tab!</p>
    </x-larastrap::tabpane>

    <x-larastrap::tabpane label="Second" button_classes="text-info">
        <p>This is the second tab!</p>
    </x-larastrap::tabpane>
</x-larastrap::tabs>
