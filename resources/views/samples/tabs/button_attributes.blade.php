<x-larastrap::tabs active="0">
    <x-larastrap::tabpane label="First" :button_attributes="['id' => 'an-id-for-tab-button', 'data-custom' => 'this-is-a-test']">
        <p>This is the first tab!</p>
    </x-larastrap::tabpane>

    <x-larastrap::tabpane label="Second">
        <p>This is the second tab!</p>
    </x-larastrap::tabpane>
</x-larastrap::tabs>
