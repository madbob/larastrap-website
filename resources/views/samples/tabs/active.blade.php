<x-larastrap::tabs>
    <x-larastrap::tabpane label="First">
        <p>This is the first tab!</p>
    </x-larastrap::tabpane>

    <x-larastrap::tabpane label="Second" active="true">
        <p>This is the second tab!</p>
    </x-larastrap::tabpane>
</x-larastrap::tabs>
