[
    'customs' => [
        'datepicker' => [
            'extends' => 'text',
            'params' => [
                'classes' => ['date'],
                'placeholder' => 'Never',
                'textappend' => '<i class="bi-calendar"></i>',

                'reviewCallback' => function($component, $params) {
                    $params['value'] = ucwords(strftime('%A %d %B %Y', $params['value']));
                    return $params;
                }
            ],
        ],
    ]
]
