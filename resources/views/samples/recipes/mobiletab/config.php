[
    'customs' => [
        'mobiletab' => [
            'extends' => 'tabpane',
            'params' => [
                'reviewCallback' => function($component, $params) {
                    if (isset($params['attributes']['icon'])) {
                        $params['label'] = sprintf('<span class="d-none d-md-inline-block me-2">%s</span><i class="%s"></i>',
                            $params['label'],
                            $params['attributes']['icon']
                        );
                    }

                    return $params;
                }
            ],
        ],
    ]
]
