[
    'customs' => [
        'mbutton' => [
            'extends' => 'button',
            'params' => [
                'color' => 'light',
                'postlabel' => ' <i class="bi-window"></i>',
            ],
        ],
    ]
]