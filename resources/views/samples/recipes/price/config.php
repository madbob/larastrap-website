[
    'customs' => [
        'price' => [
            'extends' => 'number',
            'params' => [
                'textappend' => '€',
                'attributes' => [
                    'step' => 0.01,
                ]
            ],
        ],
    ]
]
