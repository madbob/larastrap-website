[
    'customs' => [
        'hlink' => [
            'extends' => 'link',
            'params' => [
                'reviewCallback' => function($button, $params) {
                    if (isset($params['attributes']['hreflang']) == false) {
                        $params['attributes']['hreflang'] = App::getLocale();
                    }

                    return $params;
                },
            ]
        ],
    ]
]
