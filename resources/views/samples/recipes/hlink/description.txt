In multi-language websites, it may be useful to centralize the generation of <strong>hreflang</strong> attributes for each internal link using the current locale by default.
