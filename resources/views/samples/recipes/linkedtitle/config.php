[
    'customs' => [
        'linkedtitle' => [
            'extends' => 'link',
            'params' => [
                'override_classes' => ['title-link'],
                'reviewCallback' => function($node, $params) {
                    $identifier = Str::slug($params['label']);
                    $params['id'] = $identifier;
                    $params['label'] = sprintf('<h3>%s</h3>', $params['label']);
                    $params['href'] = sprintf('#%s', $identifier);
                    return $params;
                }
            ],
        ],
    ]
]
