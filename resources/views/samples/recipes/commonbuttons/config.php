[
    'customs' => [
        'ebutton' => [
            'extends' => 'link',
            'params' => [
                'color' => 'info',
                'label' => 'Edit',
            ],
        ],
        'cbutton' => [
            'extends' => 'link',
            'params' => [
                'color' => 'success',
                'label' => 'Create New',
            ],
        ],
        'dbutton' => [
            'extends' => 'link',
            'params' => [
                'color' => 'danger',
                'label' => 'Delete',
            ],
        ],
    ]
]
