You can define <a href="https://getbootstrap.com/docs/5.3/components/alerts/">Bootstrap Alerts</a> just as a Custom Element extending the Typography node. So you have far more flexibility than just the color, and define multiple types with specific CSS classes and specific behaviors.
The same applies to <a href="https://getbootstrap.com/docs/5.3/components/badge/">Bootstrap Badges</a>.
