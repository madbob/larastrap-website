<x-larastrap::suggestion>
    Use a Custom Element to define semantic alerts, to be globally configured once
    to keep consistency across the whole application.
</x-larastrap::suggestion>
