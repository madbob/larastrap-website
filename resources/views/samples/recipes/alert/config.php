[
    'customs' => [
        'suggestion' => [
            'extends' => 't',
            'params' => [
                'node' => 'div',
                'classes' => ['alert', 'alert-info', 'custom-class'],
            ],
        ],
    ]
]
