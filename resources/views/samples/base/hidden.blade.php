<x-larastrap::t node="div" classes="alert alert-info" :hidden="rand(1, 2) % 2 == 1">
    This may appear or not at every page load!
</x-larastrap::t>
