<?php

class MyAttributes
{
    public static function formatDate($button, $params)
    {
        $params['label'] = $params['label'] . ' ' . date('d/m/Y H:i:s');
        return $params;
    }
}

?>

<x-larastrap::button label="Test" :reviewCallback="[MyAttributes::class, 'formatDate']" />
