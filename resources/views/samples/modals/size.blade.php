<x-larastrap::button triggers_modal="test-size-sm" label="Open SM Modal" />
<x-larastrap::button triggers_modal="test-size-lg" label="Open LG Modal" />
<x-larastrap::button triggers_modal="test-size-responsive" label="Open Responsive Modal" />

<x-larastrap::modal id="test-size-sm" size="sm">
    <p>
        This is a small modal.
    </p>
</x-larastrap::modal>

<x-larastrap::modal id="test-size-lg" size="lg">
    <p>
        This is a large modal.
    </p>
</x-larastrap::modal>

<x-larastrap::modal id="test-size-responsive" :size="['modal-xl', 'modal-fullscreen-md-down']">
    <p>
        The size of this modal changes accordingly the current breakpoint.
    </p>
</x-larastrap::modal>
