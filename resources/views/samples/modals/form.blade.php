<x-larastrap::button triggers_modal="test-with-form" label="Open Modal" />

<x-larastrap::modal id="test-with-form">
    <x-larastrap::form>
        <x-larastrap::text label="A Sample Input" />
    </x-larastrap::form>
</x-larastrap::modal>
