<x-larastrap::button triggers_modal="test-with-buttons" label="Open Modal" />

<x-larastrap::modal id="test-with-buttons" :buttons="[['color' => 'danger', 'label' => 'Dismiss', 'attributes' => ['data-bs-dismiss' => 'modal']]]">
    <p>
        This is a modal.
    </p>
</x-larastrap::modal>
