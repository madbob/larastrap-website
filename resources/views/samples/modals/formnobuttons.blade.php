<x-larastrap::button triggers_modal="test-with-form-no-buttons" label="Open Another Modal" />

<x-larastrap::modal id="test-with-form-no-buttons">
    <x-larastrap::form keep_buttons="true">
        <x-larastrap::text label="A Sample Input in the first Form" />
    </x-larastrap::form>

    <hr>

    <x-larastrap::form keep_buttons="true">
        <x-larastrap::text label="A Sample Input in the second Form" />
    </x-larastrap::form>
</x-larastrap::modal>
