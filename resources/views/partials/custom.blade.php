@php

$path = base_path('resources/views/samples/' . str_replace('.', '/', $snippet));
$config_contents = file_get_contents($path . '/config.php');
$snippet = $snippet . '.render';

$stack = app()->make('LarastrapStack');
eval('$config = ' . $config_contents . ';');

foreach($config['customs'] as $name => $custom_config) {
    $stack->addCustomElement($name, $custom_config);
}

@endphp

@include('partials.example', [
    'snippet' => $snippet,
    'config' => $config_contents,
])
