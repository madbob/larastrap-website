@extends('layout.docs', [
    'title' => 'Temp Test',
    'claim' => 'This page is used for testing purpose only',
])

@section('docs')

<?php

$fields = [
    'productCode' => [
        'type' => 'text',
        'validation' => 'required|min:4|max:10',
        'label' => 'Product Code',
    ],
    'productLine' => [
        'type' => 'select',
        'options' => ['Motorcycles' => 'Motorcycles', 'Classic Cars' => 'Classic Cars', 'Trucks and Buses' => 'Trucks and Buses'],
        'validation' => 'required',
        'label' => 'Product Line',
    ]
];

?>

@foreach ($fields as $field => $param)
    <x-dynamic-component
        :component="sprintf('larastrap::%s', $param['type'])"
        :params="['name' => $field, 'label' => $param['label'], 'options' => $param['options'] ?? null]"
    />
@endforeach

@endsection
