<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\User;

class CommonController extends Controller
{
    public function homepage()
    {
        return view('welcome');
    }

    public function customIndex()
    {
        app()->make('LarastrapStack')->addCustomElement('jerkbutton', [
            'extends' => 'button',
            'params' => [
                'color' => 'secondary',
                'postlabel' => ' (Click Here!)',
            ],
        ]);

        return view('docs.custom-elements');
    }

    public function customExamples()
    {
        $stack = app()->make('LarastrapStack');
        $path = base_path('resources/views/samples/recipes/');
        $recipes = scandir($path);
        $data = [];

        foreach($recipes as $foldername) {
            if ($foldername == '.' || $foldername == '..') {
                continue;
            }

            $folder = $path . $foldername;
            if (is_dir($folder)) {
                $config = $folder . '/config.php';
                if (file_exists($config)) {
                    $config_contents = file_get_contents($config);
                    eval('$config = ' . $config_contents . ';');

                    foreach($config['customs'] as $name => $custom_config)
                    $stack->addCustomElement($name, $custom_config);

                    $data[] = (object) [
                        'folder' => $foldername,
                        'description' => file_get_contents($folder . '/description.txt'),
                        'config' => $config_contents,
                    ];
                }
            }
        }

        return view('docs.custom-examples', ['recipes' => $data]);
    }

    public function fakePost()
    {
        return back();
    }

    public function autoreadValidation(Request $request)
    {
        app()->make('LarastrapStack')->autoreadValidate($request, true);
        return back()->withInput();
    }

    public function autoreadSaving(Request $request)
    {
        app()->make('LarastrapStack')->autoreadValidate($request, true);

        DB::beginTransaction();
        $user = app()->make('LarastrapStack')->autoreadSave($request, User::class);
        $user->save();
        $user->load('boss', 'friends');
        DB::rollback();

        $type = $request->input('type');
        $feedback = match($type) {
            'save' => 'saved_attributes',
            'create' => 'created_attributes',
            'file' => 'file_attributes',
        };

        return back()->with($feedback, [
            'name' => $user->name,
            'email' => $user->email,
            'password' => $user->password,
            'boss' => $user->boss?->name,
            'friends' => $user->friends->map(fn($f) => $f->name)->join(','),
        ])->withInput();
    }

    public function validatedRoute(Request $request)
    {
        $request->validateWithBag('special', [
            'name' => 'required',
            'email' => 'required|email|max:255',
            'description' => 'required',
            'select' => 'required',
            'check' => 'accepted',
            'checks' => 'required',
            'radios' => 'required',
            'checklist' => 'required',
        ]);

        return redirect()->route('docs.forms');
    }

    public function validatedDotRoute(Request $request)
    {
        $request->validate([
            'person.name' => 'required',
            'person.email' => 'required|email|max:255',
        ]);

        return redirect()->route('docs.forms');
    }

    public function tempTest(Request $request)
    {
        $fields = [
            'check',
            'color',
            'date',
            'datetime',
            'email',
            'file',
            'month',
            'number',
            'password',
            'radios',
            'range',
            'select',
            'select_multiple',
            'search',
            'tel',
            'text',
            'textarea',
            'time',
            'url',
            'week'
        ];
        $rules = [
            "root.child1.child2.child3.child4.text" => 'required'
        ];
        foreach ($fields as $field) {
            $rules[$field] = 'required';
            $rules["root.child.".$field] = 'required';
            $rules["root_html.child.".$field] = 'required';
        }
        $request->validate($rules);

        return $request->all();
    }
}
