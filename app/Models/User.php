<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use MadBob\Larastrap\Integrations\AutoReadsFields;
use MadBob\Larastrap\Integrations\AutoReadOperation;

class User extends Authenticatable implements AutoReadsFields
{
    use HasFactory, Notifiable;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'attributes' => 'array',
    ];

    public function boss()
    {
        return $this->belongsTo(User::class);
    }

    public function friends()
    {
        return $this->belongsToMany(User::class, 'friends', 'user_id', 'friend_id');
    }

    public function autoreadField(string $name, Request $request): AutoReadOperation
    {
        if ($name == 'avatar') {
            Session::flash('avatar_file', $request->file('avatar')->getClientOriginalName());
            return AutoReadOperation::Managed;
        }

        return AutoReadOperation::Auto;
    }
}
