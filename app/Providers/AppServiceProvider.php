<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades;
use Illuminate\View\View;

use App\Helpers\SubMenuBuilder;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Facades\Schema::defaultStringLength(191);

        /*
            Init the component used to build the side sub-menu in each doc page.
            It is populated by the "title" Larastrap component
        */
        $this->app->singleton('submenu-builder', function ($app) {
            return new SubMenuBuilder();
        });

        /*
            To create the "View on GitLab" button displayed in each doc page
        */
        Facades\View::composer('docs.*', function (View $view) {
            $path = $view->getPath();
            $resources = resource_path('/');
            $path = str_replace($resources, 'https://gitlab.com/madbob/larastrap-website/-/blob/master/resources/', $path);
            $view->with('repo_filepath', $path);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
