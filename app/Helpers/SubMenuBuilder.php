<?php

namespace App\Helpers;

class SubMenuBuilder
{
    public $links = [];

    public function push($label, $link)
    {
        $this->links[$label] = $link;
    }
}
