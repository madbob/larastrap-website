#!/bin/bash
SESSION="larastrap"
screen -d -m -S $SESSION top
screen -S $SESSION -p 0 -X title "top"
screen -S $SESSION -X screen -t "docker" docker compose -f docker/docker-compose.yml up
echo "Waiting some seconds"
sleep 5
screen -S $SESSION -X screen -t "serve" ./artisan serve --port 8088
screen -S $SESSION -X screen -t "logs" tail -F storage/logs/laravel.log
screen -S $SESSION -X screen -t "scheduler" ./artisan schedule:work
screen -S $SESSION -X screen -t "builder" npm run dev
