<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Tests\TestCase;

class AllPagesTest extends TestCase
{
    use RefreshDatabase;

    public function testPages()
    {
        $this->seed();

        $response = $this->get('/');
        $response->assertStatus(200);

        $routes = collect(Route::getRoutes())->filter(function ($route) {
            /*
                "Donate" page has been converted to a redirect to "Contribute"
            */
            if ($route->getName() == 'docs.donate') {
                return false;
            }

            return Str::startsWith($route->uri, 'docs');
        });

        foreach($routes as $route) {
            $path = route($route->getName());
            echo $path . "\n";
            $response = $this->get($path);
            $response->assertStatus(200);
        }
    }
}
