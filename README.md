# Larastrap Website

Published at https://larastrap.madbob.org/

## How to start

1. PHP
Make sure to have:
- a recent PHP (php 8.x) with php-mysql and php-tidy modules installed
- a running docker service.

2. Run composer install

```bash
$ composer install
```

3. Copy env.dev file to .env

```bash
$ cp .env.dev .env
```

4. Setup app key
```bash
$ php artisan key:generate
```

5. Run start command, to start docker env and laravel services

```bash
$ ./start.sh
```

This command will:
- start docker images (database, redis and mailhog)
- start artisan webserver on port 8088
- start artisan scheduler
- start `npm run dev` command

6. Only for the first time, you should run database migration and seeders:
```bash
$ php artisan migrate
$ php artisan db:seed
```

The app will be served on port 8088

## Profiling

This website is usually used also for profiling and debugging Larastrap, as it includes samples of most features.

To enable profiling, change the value of `XHPROF_ENABLED` in the .env file

```
XHPROF_ENABLED=true
```

and navigate some page on your local instance: you can check collected informations in the embedded xhprof dashboard published at http://your.host/vendor/xhprof/xhprof_html/
