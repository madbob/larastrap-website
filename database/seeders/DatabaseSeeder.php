<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (User::all()->count() == 0) {
            \App\Models\User::factory(10)->create();
        }

        $has_bosses = User::where('boss_id', '!=', 0)->count();
        if ($has_bosses == 0) {
            $base = User::orderBy('id', 'asc')->take(5)->get();
            $bosses = User::orderBy('id', 'asc')->take(5)->offset(5)->get();
            foreach($base as $index => $b) {
                $b->boss_id = $bosses->slice($index, 1)->first()->id;
                $b->save();
            }
        }

        $users = User::orderBy('id', 'asc')->get();

        $has_friends = User::has('friends')->count();
        if ($has_friends == 0) {
            foreach($users as $u) {
                $friends = User::inRandomOrder()->where('id', '!=', $u->id)->take(rand(3, 5))->pluck('id');
                $u->friends()->attach($friends);
            }
        }

        $has_attributes = User::where('attributes', '!=', '')->count();
        if ($has_attributes == 0) {
            foreach($users as $u) {
                $u->attributes = ['external_id' => Str::random(10)];
                $u->save();
            }
        }
    }
}
